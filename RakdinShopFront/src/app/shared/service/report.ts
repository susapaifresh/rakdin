import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { productReceiptReport, productOrderReport } from '../model/report';
import { RequestOptions } from '@angular/http';
@Injectable()
export class ReportService {

  private API_PATH: string;
  private headers: Headers;
  public currentUser: any;
  public userName: string = '';
  public password: string = '';
  public user: any;

  constructor(private http: Http, private global: Global) {
    this.API_PATH = `${global.geturl()}`;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
  }


  private _getHeaders(): Headers {
    let header = new Headers({
      'Content-Type': 'application/json'
    });

    return header;
  }

  getReport(fromdate, todate, orderID, customerID) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.post(this.API_PATH + 'OrderReport', JSON.stringify({ 'startdate': fromdate, 'enddate': todate, 'orderID': orderID, 'customerID': customerID }), options).map(res => res.json() as productOrderReport[]);
  }
  getReceiptReport(fromdate, todate, orderID) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.post(this.API_PATH + 'OrderReceiptReport', JSON.stringify({ 'startdate': fromdate, 'enddate': todate, 'orderID': orderID }), options).map(res => res.json() as productReceiptReport[]);
  }
  getReportDetail(id) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.get(this.API_PATH + 'OrderReport/' + id, options).map(res => res.json() as productOrderReport[]);
  }

  updateReport(id, status, email, updateby) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.post(this.API_PATH + 'UpdateOrderReport', JSON.stringify({ 'id': id, 'status': status, 'email': email, 'updateby': updateby }), options).map(res => res.json());
  }
}