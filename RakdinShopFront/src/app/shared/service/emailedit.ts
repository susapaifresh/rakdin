import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { EmailConfig, EmailConfigEmailListRole, EmailConfigDetail, CreateEmailConfig } from '../model/emailconfig';
@Injectable()
export class EmailEditService {

  private API_PATH: string;
  private headers: Headers;
  public currentUser: any;
  public userName: string = '';
  public password: string = '';
  public user: any;

  constructor(private http: Http, private global: Global) {
    this.API_PATH = `${global.geturl()}`;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
  }


  private _getHeaders(): Headers {
    let header = new Headers({
      'Content-Type': 'application/json'
    });

    return header;
  }

  getEmailConfigList() {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.get(this.API_PATH + 'EmailConfig', options).map(res => res.json() as EmailConfig[]);
  }
  getEmailConfigEmailList(id: string) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.get(this.API_PATH + 'getEmailListByRole/' + id, options).map(res => res.json() as EmailConfigEmailListRole);
  }
  getEmailConfigByID(id: number) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.get(this.API_PATH + 'EmailConfig/' + id, options).map(res => res.json() as EmailConfigDetail);
  }
  updateEmailConfig(email: EmailConfigDetail) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.put(this.API_PATH + 'EmailConfig/update', email, options).map(res => res.json());
  }
  deleteEmailConfig(id: number) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.delete(this.API_PATH + 'EmailConfig/delete/' + id, options).map(res => res.json());
  }
  createEmailConfig(createEmail: CreateEmailConfig) {
    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));

    //return this.http.post(this.API_PATH + 'login', { headers: this.headers, body: { 'username': username, 'password': password } });
    return this.http.post(this.API_PATH + 'EmailConfig/add', JSON.stringify(createEmail), options).map(res => res.json());
  }
}