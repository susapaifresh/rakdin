import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { CustomerUpdateAddress, CustomerUpdateProfile, CustomerListActive, CustomerRegister, CustomerObj } from '../model/customer';
import { md5 } from '../service/md5';

@Injectable()
export class CustomerService {

    private API_PATH: string;
    private controllerName: string;
    private headers: Headers;

    public currentUser: any;
    public userName: string = '';
    public password: string = '';
    public user: any;

    constructor(private http: Http, private global: Global) {

        this.API_PATH = `${global.geturl()}`;
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        // if (this.user) {
        //   this.getheader();
        //   this.headers.append('Authorization', 'Basic ' + btoa(`${this.userName}:${this.password}`));
        // }
    }


    private _getHeaders(): Headers {
        let header = new Headers({
            'Content-Type': 'application/json'
        });

        return header;
    }
    register(email: string, inputPassword: string,
        name: string, tel: string
        , homenumber: string, subdistrict: string
        , district: string, province: string
        , postcode: string, phone: string) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        let passWordEncyptBase64 = btoa(inputPassword);
        let passWordEncyptMD5 = md5(passWordEncyptBase64);

        return this.http.post(this.API_PATH + 'customerasync'
            , JSON.stringify({
                'email': email
                , 'nameuser': name
                , 'password': passWordEncyptMD5
                , 'phone': phone
                , 'tel': tel
                , 'homenumber': homenumber
                , 'subdistrict': subdistrict
                , 'district': district
                , 'province': province
                , 'postcode': postcode
            }), options).map(res => res.json() as CustomerRegister);
    }
    getCustomerDetail(id) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        return this.http.get(this.API_PATH + 'customerasync/' + id, options).map(res => res.json() as CustomerObj);
    }
    orderProduct(orderobj) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        return this.http.post(this.API_PATH + 'order', JSON.stringify(orderobj), options).map(res => res.json() as CustomerObj);
    }
    forgetPassword(email) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        return this.http.get(this.API_PATH + 'ForgotAsync/' + email, options).map(res => res.json());
    }
    checkUrl(url) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        return this.http.get(this.API_PATH + 'ChangePassword/' + url, options).map(res => res.json() as CustomerObj);
    }
    changePassword(id, password) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        let passWordEncyptBase64 = btoa(password);
        let passWordEncyptMD5 = md5(passWordEncyptBase64);
        return this.http.post(this.API_PATH + 'ChangePassword', JSON.stringify({ 'id': id, 'password': passWordEncyptMD5 }), options).map(res => res.json());
    }
    getListOrderActive(id) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        return this.http.get(this.API_PATH + 'OrderReportGetList/' + id, options).map(res => res.json() as CustomerListActive[]);
    }
    updateProfile(id, customerDetailUpdate: CustomerUpdateProfile) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.put(this.API_PATH + 'customerasync/' + id, JSON.stringify(customerDetailUpdate), options).map(res => res.json());
    }
    updateAddress(id, customerAddressUpdate: CustomerUpdateAddress) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.put(this.API_PATH + 'updateaddress/' + id, JSON.stringify(customerAddressUpdate), options).map(res => res.json());
    }
}