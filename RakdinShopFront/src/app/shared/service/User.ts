import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { BaseService } from './Base';
import { UserLogin } from '../model/user';
import { RequestOptions } from '@angular/http';
import { md5 } from '../service/md5';

@Injectable()
export class UserService {

  private API_PATH: string;
  private controllerName: string;
  private headers: Headers;

  public currentUser: any;
  public userName: string = '';
  public password: string = '';
  public user: any;

  constructor(private http: Http, private global: Global) {

    this.API_PATH = `${global.geturl()}`;
    this.headers = new Headers({ 'Content-Type': 'application/json' });
    // if (this.user) {
    //   this.getheader();
    //   this.headers.append('Authorization', 'Basic ' + btoa(`${this.userName}:${this.password}`));
    // }
  }


  private _getHeaders(): Headers {
    let header = new Headers({
      'Content-Type': 'application/json'
    });

    return header;
  }


  login(email: string, password: string) {

    let options = new RequestOptions({
      headers: this._getHeaders()
    });
    //   headers.append('Authorization', 'Basic ' + btoa(temp));
    let passWordEncyptBase64 = btoa(password);
    let passWordEncyptMD5 = md5(passWordEncyptBase64);
     return this.http.post(this.API_PATH + 'customerlogin', JSON.stringify({ 'email': email, 'password': passWordEncyptMD5 }), options).map(res => res.json() as UserLogin);
  }
}