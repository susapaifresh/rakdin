import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { ReceiptModel } from '../model/receipt';

@Injectable()
export class ReceiptService {

    private API_PATH: string;
    private headers: Headers;

    constructor(private http: Http, private global: Global) {

        this.API_PATH = `${global.geturl()}`;
        this.headers = new Headers({ 'Content-Type': 'application/json' });
    }


    private _getHeaders(): Headers {
        let header = new Headers({
            'Content-Type': 'application/json'
        });

        return header;

    }
    addReceipt(receipt: ReceiptModel) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.post(this.API_PATH + 'OrderReceipt'
            , JSON.stringify(receipt), options).map(res => res.json());
    }
 
}