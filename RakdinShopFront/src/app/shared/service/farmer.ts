import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { FarmerResult, Farmer } from '../model/farmer';

@Injectable()
export class FarmerService {

    private API_PATH: string;
    private headers: Headers;

    constructor(private http: Http, private global: Global) {

        this.API_PATH = `${global.geturl()}`;
        this.headers = new Headers({ 'Content-Type': 'application/json' });
    }


    private _getHeaders(): Headers {
        let header = new Headers({
            'Content-Type': 'application/json'
        });

        return header;
    }
    addFarmer(name: string, detail: string,
        sector: string, imageprofile: string) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.post(this.API_PATH + 'FarmerAsync'
            , JSON.stringify({
                'name': name
                , 'detail': detail
                , 'sector': sector
                , 'imageprofile': imageprofile
            }), options).map(res => res.json() as FarmerResult);
    }
    getAllFarmer() {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'FarmerAsync', options).map(res => res.json() as Farmer[]);
    }
    getFarmer(id: string) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'FarmerAsync/'+id, options).map(res => res.json() as Farmer);
    }
}