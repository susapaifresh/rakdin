import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Global } from './global';

@Injectable()
export class BaseService {
	  private API_PATH: string; 
	   constructor(controllerName : string ,global : Global) {
     this.API_PATH = global.geturl() + controllerName ;
  } 
}