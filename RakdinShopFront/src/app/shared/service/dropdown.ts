
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { DropDown } from '../model/dropdown';

@Injectable()
export class DropDownService {

    private API_PATH: string;
    private headers: Headers;

    constructor(private http: Http, private global: Global) {

        this.API_PATH = `${global.geturl()}`;
        this.headers = new Headers({ 'Content-Type': 'application/json' });
    }


    private _getHeaders(): Headers {
        let header = new Headers({
            'Content-Type': 'application/json'
        });

        return header;

    }

    getUnitDropDown() {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'unitdropdown', options).map(res => res.json() as DropDown[]);
    }
    getFarmerDropDown() {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'farmerdropdown', options).map(res => res.json() as DropDown[]);
    }
    getGroupDropDown() {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'groupdropdown', options).map(res => res.json() as DropDown[]);
    }
}