import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { ProductUpdate,product, ProductRegister, productResult, ProductDelete } from '../model/product';

@Injectable()
export class ProductService {

    private API_PATH: string;
    private headers: Headers;

    constructor(private http: Http, private global: Global) {

        this.API_PATH = `${global.geturl()}`;
        this.headers = new Headers({ 'Content-Type': 'application/json' });
    }


    private _getHeaders(): Headers {
        let header = new Headers({
            'Content-Type': 'application/json'
        });

        return header;

    }
    addproduct(product: product) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.post(this.API_PATH + 'ProductAsync'
            , JSON.stringify(product), options).map(res => res.json() as ProductRegister);
    }
    getProductDetail(id) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'ProductAsync/' + id, options).map(res => res.json() as productResult);
    }
    getProductAll() {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'ProductAsync', options).map(res => res.json() as productResult[]);
    }
    getProductByGroupAll(groupID) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.get(this.API_PATH + 'ProductGroup/' + groupID, options).map(res => res.json() as productResult[]);
    }
    deleteProduct(id) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.delete(this.API_PATH + 'ProductAsync/' + id, options).map(res => res.json() as ProductDelete[]);
    }
    updateProduct(id, product: product) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.put(this.API_PATH + 'ProductAsync/' + id, JSON.stringify(product), options).map(res => res.json() as ProductUpdate);
    }
}