import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Global } from './global';
import { RequestOptions } from '@angular/http';
import { FarmerResult, Farmer } from '../model/farmer';

@Injectable()
export class FirstPageService {

    private API_PATH: string;
    private headers: Headers;

    constructor(private http: Http, private global: Global) {

        this.API_PATH = `${global.geturl()}`;
        this.headers = new Headers({ 'Content-Type': 'application/json' });
    }


    private _getHeaders(): Headers {
        let header = new Headers({
            'Content-Type': 'application/json'
        });

        return header;
    }
    addFirstPage(aboutUs: string, imageBanner1: string, imageBanner2: string, imageBanner3: string, imageBanner4: string) {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });

        return this.http.post(this.API_PATH + 'AddFirstPage'
            , JSON.stringify({ 'aboutUs': aboutUs
                , 'imageBanner1': imageBanner1
                , 'imageBanner2': imageBanner2
                , 'imageBanner3': imageBanner3
                , 'imageBanner4': imageBanner4
            }), options).map(res => res.json() as AddFirstPageResult);
    }
    getFirstPage() {
        let options = new RequestOptions({
            headers: this._getHeaders()
        });
        return this.http.get(this.API_PATH + 'AddFirstPage', options).map(res => res.json() as FirstPageResult);
    }
}

export class AddFirstPageResult {
    status: number;
    text: string;
    obj: string;
}
export class FirstPageResult {
    aboutUs: string;
    imageBanner1: string;
    imageBanner2: string;
    imageBanner3: string;
    imageBanner4: string;
}