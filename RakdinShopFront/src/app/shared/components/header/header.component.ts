import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
// import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { productCart } from '../../model/cart';
import { CustomerService } from '../../service/customer';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    nameuser: string = 'null';
    isLogin = false;
    isAdmin = false;
    itemIncart = 0;
    itemIncartObj = [];
    sumprice = 0;

    //forgetpassword
    emailforgot = '';


    /////modal
    modalMessage;
    modalResult;
    modalIcon;
    colorH;

    constructor(public router: Router, private customerService: CustomerService) {

    }
    showCart() {
        // localStorage.removeItem('cart');
        this.showModal();
    }
    updateheader() {
        this.ngOnInit();
    }

    ngOnInit() {
        if (localStorage.getItem('nameuser') != null) {
            this.nameuser = localStorage.getItem('nameuser');
            this.isLogin = true;
            this.checkRoleUser();
        }
        this.updateCart();
    }

    checkRoleUser() {
        if (localStorage.getItem('role') != null) {
            if (localStorage.getItem('role') == "1") {
                this.isAdmin = true;
            }
        } else {

        }
    }
    updateCart() {
        let cartAll = JSON.parse(localStorage.getItem('cart'));
        this.itemIncartObj = cartAll;
        if (cartAll != null) {
            this.itemIncart = cartAll.length;
        } else {
            this.itemIncart = 0;
        }
    }
    deleteItemInCart(id) {
        let tmp = [];
        for (let i = 0; i < this.itemIncartObj.length; i++) {
            if (id != this.itemIncartObj[i].id) {
                tmp.push(this.itemIncartObj[i]);
            }
        }
        if (tmp == null) {
            localStorage.removeItem('cart');
        } else {
            this.itemIncartObj = tmp;
            localStorage.removeItem('cart');
            localStorage.setItem('cart', JSON.stringify(tmp));
        }
        this.updateCart();
        this.sumpriceorder();
    }
    register() {
        localStorage.clear();
        this.router.navigate(['/register']);
    }
    login() {
        this.router.navigate(['/login']);
    }
    logout() {
        this.showModalLogout();

    }
    order() {
        this.closeModal();
        this.router.navigate(['/order']);
    }
    sumpriceorder() {
        this.sumprice = 0;
        for (let i = 0; i < this.itemIncartObj.length; i++) {
            this.sumprice = this.sumprice + (this.itemIncartObj[i].price * this.itemIncartObj[i].quantity);
        }
    }
    changeQty(item, value) {
        for (let i = 0; i < this.itemIncartObj.length; i++) {
            if (item.id == this.itemIncartObj[i].id) {
                this.itemIncartObj[i].quantity = value.target.value;
            }
        }
        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(this.itemIncartObj));
        this.sumpriceorder();
    }
    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }
    sendForgotPassword() {
        this.customerService.forgetPassword(this.emailforgot).subscribe(data => {
            this.modalMessage = "";
            this.modalResult = "ลิ้งค์สำหรับเปลี่ยนรหัสผ่านได้ถูกส่งไปยังอีเมลของท่าน";
            this.modalIcon = "fas fa-check-circle iGreen";
            this.colorH = "iGreen";
            this.closeModalForgotPassword();
            this.showModalAlert();
        },
            error => {
                this.modalMessage = "มีข้อผิดพลาดไม่สามารถรีเซ็ทรหัสผ่านได้โปรดตรวจสอบอีเมล";
                this.modalResult = "มีข้อผิดพลาด !";
                this.modalIcon = "fas fa-times-circle iRed";
                this.colorH = "iRed";
                this.closeModalForgotPassword();
                this.showModalAlert();
            });
    }
    updateCartAnimation() {
        let animation2 = document.getElementById('acart');

        animation2.classList.remove('img-tmp2');
        animation2.style.animation = 'none';
        animation2.offsetHeight; /* trigger reflow */
        animation2.style.animation = null;
        animation2.classList.add('img-tmp2');
    }
    forgotpassword() {
        this.showModalForgotPassword();
    }
    showModalForgotPassword() {
        document.getElementById("pwdModal").click()
    }
    closeModalForgotPassword() {
        document.getElementById("pwdModal").click()
    }
    showModal() {
        this.sumpriceorder();
        document.getElementById("btnModalCart").click()
    }
    closeModal() {
        document.getElementById("btnModalCart").click()

    }
    showModalLogout() {
        document.getElementById("btnModalLogout").click()
    }
    closeModalLogout() {
        document.getElementById("btnModalLogout").click()
        localStorage.clear();
        this.router.navigate(['/index']);
        location.reload();
    }

    showModalAlert() {
        document.getElementById("btnModalAlert").click()
    }
    closeModalAlert() {
        document.getElementById("btnModalAlert").click()
    }
}
