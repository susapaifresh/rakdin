export class CustomerRegister {
    status: number;
    text: string;
    obj: string;
}
export class CustomerObj {
    id: number;
    email: string;
    password: string;
    nameuser: string;
    phone: string;
    tel: string;
    homenumber: string;
    subdistrict: string;
    district: string;
    province: string;
    postcode: string;
    homenumber2: string;
    subdistrict2: string;
    district2: string;
    province2: string;
    postcode2: string;
    homenumber3: string;
    subdistrict3: string;
    district3: string;
    province3: string;
    postcode3: string;
    role: number;
}
export class CustomerUpdateProfile {
    nameuser: string;
    phone: string;
    tel: string;
}

export class CustomerUpdateAddress {
    homenumber: string;
    subdistrict: string;
    district: string;
    province: string;
    postcode: string;
    addNo: string;
}

export class CustomerListActive {
    id: string;
}