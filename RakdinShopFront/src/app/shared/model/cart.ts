export class productCart {
  id: number;
  productname: string;
  price: number;
  group: string;
  picture: string;
  unit: string;
  promotion: string;
  quantity: number;
}

export class productOrder {
  id: string;
  idproduct: number;
  productname: string;
  price: number;
  group: string;
  unit: string;
  quantity: number;
  userid: number;
  address: string;
}