export class EmailConfig {
    TEMPLATE_ID: number;
    TEMPLATE_NAME: string;
    DESCRIPTION: string;
}
export class EmailConfigEmailListRole {
    Emails: string;
    ROL_NAME: string;
    USR_ROLE: string;
}
export class EmailConfigDetail {
    TEMPLATE_ID: number;
    TEMPLATE_NAME: string;
    DESCRIPTION: string;
    SUBJECT: string;
    BODY: string;
    USER_ROLE: string;
    MAIL_CC: string;
    SEND_TO: string;
}
export class CreateEmailConfig {
    TEMPLATE_NAME: string;
    DESCRIPTION: string;
    SUBJECT: string;
    BODY: string;
    MAIL_CC: string;
    USER_ROLE: string;
    SEND_TO: string;
}
