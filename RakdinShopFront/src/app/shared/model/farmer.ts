export class FarmerResult {
    status: number;
    text: string;
    obj: string;
}
export class Farmer {
    id: number;
    name: string;
    detail: string;
    sector: string;
    imageprofile:string;
}