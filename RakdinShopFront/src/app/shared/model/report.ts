export class productOrderReport {
    id: string;
    idproduct: number;
    productname: string;
    price: number;
    group: string;
    unit: string;
    quantity: number;
    userid: number;
    nameuser: string;
    createdate: Date;
    updatedate: Date;
    status:string;
    email:string;
    address:string;
}
export class productReceiptReport {
    id: number;
    orderid: string;
    amount: string;
    receiptimage: string;
    remark: string;
    datereceipt: Date;
    status: number;
    userid: number;
    username: string;
    nameuser: string;
    createdate: Date;
    updatedate: Date;
}