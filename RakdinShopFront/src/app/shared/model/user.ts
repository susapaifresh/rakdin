export class MenuAccess {
    ITM_ITEM: string;
    ITM_NAME: string;
    USR_USERNAME: string;
}

export class UserLogin {
    status: number;
    text: string;
    obj: string;
}
