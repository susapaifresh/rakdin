export class product {
  id: number;
  productname: string;
  promotion: string;
  price: number;
  price2: number;
  group: number;
  picture: string;
  unit: number;
  ownerfarmer: number;
  isactive: boolean;
}
export class productResult {
  id: number;
  productname: string;
  promotion: string;
  groupprecis: string;
  price: number;
  price2: number;
  group: string;
  picture: string;
  unit: string;
  ownerfarmer: string;
  ownerfarmerid: number;
  groupid: number;
  unitid: number;
  isactive: boolean;
}
export class ProductRegister {
  status: number;
  text: string;
  obj: string;
}
export class ProductDelete {
  status: number;
  text: string;
  obj: string;
}
export class ProductUpdate {
  status: number;
  text: string;
  obj: string;
}