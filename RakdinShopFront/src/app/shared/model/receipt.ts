export class ReceiptModel {
    id: number;
    orderid: string;
    receiptimage: string;
    remark: string;
    amount: string;
    userid: number;
    datereceipt: Date;
    createdate: Date;
    updatedate: Date;
}