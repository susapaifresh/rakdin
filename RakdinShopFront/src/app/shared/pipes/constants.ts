export class Constants {
    static readonly DATE_QUERY = 'yyyy-MM-dd 00:00:01';
    static readonly DATE_QUERY2 = 'yyyy-MM-dd 23:59:00';
    static readonly DATE_QUERY3 = 'yyyy-MM-dd';
    static readonly DATE_QUERY4 = 'dd/MMM/yyyy HH:mm:00';

    // static readonly DATE_QUERY2 = 'yyyy-MM-dd';

    static readonly DATE_FMT = 'dd/MMM/yyyy';
    static readonly DATE_TIME_FMT = `${Constants.DATE_FMT} hh:mm:ss`;
}