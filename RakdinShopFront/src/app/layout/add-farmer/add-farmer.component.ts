
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FarmerService } from '../../shared/service/farmer';
import { Farmer, FarmerResult } from '../../shared/model/farmer';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-add-farmer',
  templateUrl: './add-farmer.component.html',
  styleUrls: ['./add-farmer.component.scss']
})
export class AddFarmerComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  showLoader = false
  labelText = "test";
  farmername;
  farmersector;
  form: FormGroup;
  url = '';
  imageBanner = "";
  farmerDetail = '';
  RoleDropDowns: DropdownSector[] = [];
  constructor(private farmerservice: FarmerService,private router: Router) {
    let north = new DropdownSector();
    north.id = 0;
    north.name = "เหนือ";
    let t = new DropdownSector();
    t.id = 1;
    t.name = "กลาง";
    let m = new DropdownSector();
    m.id = 2;
    m.name = "ตะวันออก";
    let s = new DropdownSector();
    s.id = 3;
    s.name = "ใต้";

    this.RoleDropDowns.push(north);
    this.RoleDropDowns.push(t);
    this.RoleDropDowns.push(m);
    this.RoleDropDowns.push(s);
  }

  ngOnInit() {

  }
  onRemoved(e) {
    this.imageBanner = '';
  }
  onUploadFinished(e) {
    this.imageBanner = e.src;
  }
  save() {
    this.showLoader = true

    this.farmerDetail = document.querySelector(".ql-editor").innerHTML;
    this.farmerservice.addFarmer(this.farmername, this.farmerDetail, this.farmersector, this.imageBanner).subscribe(
      data => {
        this.showLoader = false

        console.log(data);
        this.showLoader = false
        this.modalIcon = "fas fa-check-circle iGreen"
        this.colorH = "iGreen"
        this.modalResult = "Success !"
        this.modalMessage = data.text;
        this.showModal();
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    location.reload();
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let target: any = event.target; //<-- This (any) will tell compiler to shut up!
        let content: string = target.result;
        this.url = content;
      }
    }
  }
}
export class DropdownSector {
  id: number;
  name: string;
}