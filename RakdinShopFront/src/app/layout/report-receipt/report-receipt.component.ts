import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ReportService } from '../../shared/service/report';
import { DateFormatPipe, DateFormatPipe2 } from '../../shared/pipes/DateFormatPipe';
import { productReceiptReport } from '../../shared/model/report';

export interface Status {
  value: string;
  viewValue: string;
}
export class CustomerTmp {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-report-receipt',
  templateUrl: './report-receipt.component.html',
  styleUrls: ['./report-receipt.component.scss'],
  animations: [routerTransition()]
})

export class ReportReceiptComponent implements OnInit {
  length: number;
  dataSource: MatTableDataSource<productReceiptReport>;
  fromdate: Date;
  todate: Date;
  userInput: string;
  dataTmp: productReceiptReport[];
  customerInput: string;
  statusInput: string;
  displayedColumns: string[] = ['เลขที่คำสั่งซื้อ', 'ผู้สั่งซื้อ', 'จำนวนเงิน', 'ข้อความจากลูกค้า', 'วันเวลาที่ทำการโอน', 'สถานะ', 'วันที่เวลาที่ทำการแจ้ง', 'รูปสลิป'];
  dataExport = [];
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;
  /////modal image 
  imagePic = "";


  showLoader = false;
  orderID = "";

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private reportservice: ReportService) {
    this.fromdate = new Date();
    this.todate = new Date();
  }

  ngOnInit() {
  }

  search() {
    this.showLoader = true

    this.reportservice.getReceiptReport(new DateFormatPipe('en').transform(this.fromdate), new DateFormatPipe2('en').transform(this.todate), this.orderID.trim()).subscribe(
      data => {
        this.showLoader = false
        this.dataSource = new MatTableDataSource<productReceiptReport>(data);
        this.dataSource.paginator = this.paginator;
        this.dataTmp = data;
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });

  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
  }
  showModalImage(imagePic) {
    this.imagePic = imagePic;
    document.getElementById("btnModalImage").click()
  }
  closeModalImage() {
    document.getElementById("btnModalImage").click()
  }
  setStatusToText(id) {
    switch (id) {
      case 0: {
        return 'ยังไม่ชำระ';
      }

      case 1: {
        return 'ยินยันการชำระเงิน';
      }

      case 2: {
        return 'จัดส่งสินค้า';
      }
      case 3: {
        return 'ผู้สั่งได้รับสินค้าเรัยบร้อย';
      }

      default: {
        return 'ยังไม่ชำระ';
      }
    }
  }
}