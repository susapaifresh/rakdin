
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/service/product';
import { Router, ActivatedRoute } from '@angular/router';
import { productResult } from '../../shared/model/product';
import { productCart } from '../../shared/model/cart';

@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  dataProduct: productResult = new productResult;
  addProductToCart: productCart[] = [];
  //input
  quantity = 1;
  detailFarmer;
  showLoader = false
  test = '';
  constructor(private route: ActivatedRoute, private productService: ProductService, private router: Router) {

  }

  ngOnInit() {
    this.showLoader = true
    this.route.params.subscribe(params => {
      let id: string = params['id'];
      this.productService.getProductDetail(id).subscribe(
        data => {
          this.quantity = 1;

          this.showLoader = false
          this.dataProduct = data;
        },
        error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
    });
  }

  addProduct() {
    let tmpAddCart = new productCart();

    tmpAddCart.id = this.dataProduct.id;
    tmpAddCart.productname = this.dataProduct.productname;
    tmpAddCart.price = this.dataProduct.price;
    tmpAddCart.group = this.dataProduct.group;
    tmpAddCart.picture = this.dataProduct.picture;
    tmpAddCart.unit = this.dataProduct.unit;
    tmpAddCart.promotion = this.dataProduct.promotion;
    tmpAddCart.quantity = this.quantity;
    this.addProductToCart = [];
    this.addProductToCart.push(tmpAddCart);
    //localStorage.setItem('cart', JSON.stringify([this.addProductToCart]));
    this.checkProductInCart();
  }

  checkProductInCart() {
    let haveInCart = false;
    let cartAll = JSON.parse(localStorage.getItem('cart'));
    //check have item in cart

    if (cartAll == null) {
      localStorage.setItem('cart', JSON.stringify(this.addProductToCart));
    } else {
      for (let i = 0; i < cartAll.length; i++) {
        if (this.addProductToCart[0].id == cartAll[i].id) {
          cartAll[i].quantity = cartAll[i].quantity + this.addProductToCart[0].quantity;
          haveInCart = true;
        }
      }
      if (!haveInCart) {
        cartAll[cartAll.length] = this.addProductToCart[0];

        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(cartAll));
      } else {
        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(cartAll));
      }
    }
    let animation = document.getElementById('el');
    animation.classList.remove('img-tmp');
    animation.style.animation = 'none';
    animation.offsetHeight; /* trigger reflow */
    animation.style.animation = null;

    animation.classList.add('img-tmp');
    const e2: HTMLElement = document.getElementById('cartAnimation');
    e2.click();
    const el: HTMLElement = document.getElementById('cartcontrol');
    el.click();
    this.quantity = 1;
  }

  quantityChange() {
    if (this.quantity > 100) {
      this.quantity = 100;
    }
  }
  minus() {
    if (this.quantity > 1) {
      this.quantity--;
    }
  }
  plus() {
    this.quantity++;
  }
  showModalCart() {
    document.getElementById("btnModalCart").click()
  }
  closeModalCart() {
    document.getElementById("btnModalCart").click()
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
  }
}