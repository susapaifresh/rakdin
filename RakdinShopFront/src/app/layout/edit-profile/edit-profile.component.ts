
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MenuAccess } from '../../shared/model/user';
import { CustomerService } from '../../shared/service/customer';
import { CustomerUpdateAddress, CustomerUpdateProfile } from '../../shared/model/customer';
declare var $: any;

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})

export class EditProfileComponent implements OnInit {
  showLoader = false

  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;
  statusModalCheck = 0;

  model: any = {};
  loading = false;
  returnUrl: string;
  values = '';
  display = 'none';
  listmenutmp: MenuAccess[];
  checkPasswordMatch = true;

  showAddress1 = false;
  showAddress2 = false;
  showAddress3 = false;

  homenumberadd;
  subdistrictadd;
  districtadd;
  provinceadd;
  postcodeadd;

  addAddressNo = "1";
  deleteAddressNo = "1";
  customerUpdateDetail: CustomerUpdateProfile = new CustomerUpdateProfile;
  customerUpdateAddress: CustomerUpdateAddress = new CustomerUpdateAddress;

  constructor(private route: ActivatedRoute,
    private router: Router, private dialog: MatDialog, private CustomerService: CustomerService) {

  }

  ngOnInit() {
    this.CustomerService.getCustomerDetail(localStorage.getItem("id")).subscribe(data => {
      if (data != null) {
        this.model.email = data.email
        this.model.name = data.nameuser
        this.model.tel = data.phone
        this.model.phone = data.tel

        if (data.homenumber != "-" && data.homenumber != "") {
          this.showAddress1 = true;
        }
        if (data.homenumber2 != "-" && data.homenumber2 != "") {
          this.showAddress2 = true;
        }
        if (data.homenumber3 != "-" && data.homenumber3 != "") {
          this.showAddress3 = true;
        }

        this.model.homenumber = data.homenumber
        this.model.subdistrict = data.subdistrict
        this.model.district = data.district
        this.model.province = data.province
        this.model.postcode = data.postcode

        this.model.homenumber2 = data.homenumber2
        this.model.subdistrict2 = data.subdistrict2
        this.model.district2 = data.district2
        this.model.province2 = data.province2
        this.model.postcode2 = data.postcode2

        this.model.homenumber3 = data.homenumber3
        this.model.subdistrict3 = data.subdistrict3
        this.model.district3 = data.district3
        this.model.province3 = data.province3
        this.model.postcode3 = data.postcode3
      }

    }, error => {

    })
  }

  deleteAddress() {
    if (this.showAddress1 || this.showAddress2 || this.showAddress3) {
      if (this.deleteAddressNo == "1") {
        this.customerUpdateAddress.homenumber = "-";
        this.customerUpdateAddress.subdistrict = "-";
        this.customerUpdateAddress.district = "-";
        this.customerUpdateAddress.province = "-";
        this.customerUpdateAddress.postcode = "-";
        this.customerUpdateAddress.addNo = "1"
      }
      if (this.deleteAddressNo == "2") {
        this.customerUpdateAddress.homenumber = "-";
        this.customerUpdateAddress.subdistrict = "-";
        this.customerUpdateAddress.district = "-";
        this.customerUpdateAddress.province = "-";
        this.customerUpdateAddress.postcode = "-";
        this.customerUpdateAddress.addNo = "2"
      }
      if (this.deleteAddressNo == "3") {
        this.customerUpdateAddress.homenumber = "-";
        this.customerUpdateAddress.subdistrict = "-";
        this.customerUpdateAddress.district = "-";
        this.customerUpdateAddress.province = "-";
        this.customerUpdateAddress.postcode = "-";
        this.customerUpdateAddress.addNo = "3"
      }

      this.CustomerService.updateAddress(localStorage.getItem("id"), this.customerUpdateAddress).subscribe(data => {
        if (data == 200) {
          this.showLoader = false
          this.modalIcon = "fas fa-check-circle iGreen"
          this.colorH = "iGreen"
          this.modalResult = "ลบที่อยู่สำเร็จ!"
          this.showModal();
          location.reload();

        } else {
          this.showLoader = false
          this.modalMessage = "ไม่สามารถลบที่อยู่";
          this.modalResult = "มีข้อผิดพลาด !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        }
  
      }, error => {
        this.showLoader = false
  
        console.log("can't delete")
        this.modalMessage = "ไม่สามารถอัพเดทข้อมูลส่วนตัว";
        this.modalResult = "มีข้อผิดพลาด !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      })
    } else {
      console.log("can't delete")
      this.modalMessage = "ไม่สามารถลบที่อยู่ได้ท่านต้องมีที่อยู่อย่างน้อย 1 ในระบบ";
      this.modalResult = "มีข้อผิดพลาด !";
      this.modalIcon = "fas fa-times-circle iRed";
      this.colorH = "iRed";
      this.showModal();
    }
  }

  updateProfile() {
    this.showLoader = true
    this.customerUpdateDetail.nameuser = this.model.name;
    this.customerUpdateDetail.tel = this.model.tel;
    this.customerUpdateDetail.phone = this.model.phone;

    this.CustomerService.updateProfile(localStorage.getItem("id"), this.customerUpdateDetail).subscribe(data => {
      if (data == 200) {
        this.showLoader = false
        this.modalIcon = "fas fa-check-circle iGreen"
        this.colorH = "iGreen"
        this.modalResult = "อัพเดทข้อมูลส่วนตัวสำเร็จ!"
        this.showModal();
        location.reload();

      } else {
        this.showLoader = false
        this.modalMessage = "ไม่สามารถอัพเดทข้อมูลส่วนตัว";
        this.modalResult = "มีข้อผิดพลาด !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      }

    }, error => {
      this.showLoader = false

      console.log("can't delete")
      this.modalMessage = "ไม่สามารถอัพเดทข้อมูลส่วนตัว";
      this.modalResult = "มีข้อผิดพลาด !";
      this.modalIcon = "fas fa-times-circle iRed";
      this.colorH = "iRed";
      this.showModal();
    })
  }

  add() {
    this.customerUpdateAddress.homenumber = this.model.homenumberadd;
    this.customerUpdateAddress.subdistrict = this.model.subdistrictadd;
    this.customerUpdateAddress.district = this.model.districtadd;
    this.customerUpdateAddress.province = this.model.provinceadd;
    this.customerUpdateAddress.postcode = this.model.postcodeadd;
    this.customerUpdateAddress.addNo = this.addAddressNo;

    this.CustomerService.updateAddress(localStorage.getItem("id"), this.customerUpdateAddress).subscribe(data => {
      if (data == 200) {
        this.showLoader = false
        this.modalIcon = "fas fa-check-circle iGreen"
        this.colorH = "iGreen"
        this.modalResult = "อัพเดทที่อยู่สำเร็จ!"
        this.showModal();
        location.reload();

      } else {
        this.showLoader = false
        this.modalMessage = "ไม่สามารถอัพเดทที่อยู่";
        this.modalResult = "มีข้อผิดพลาด !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      }

    }, error => {
      this.showLoader = false

      console.log("can't delete")
      this.modalMessage = "ไม่สามารถอัพเดทข้อมูลส่วนตัว";
      this.modalResult = "มีข้อผิดพลาด !";
      this.modalIcon = "fas fa-times-circle iRed";
      this.colorH = "iRed";
      this.showModal();
    })
  }

  addAddress() {
    if (this.showAddress1 == false) {
      this.showModalAdd()
      this.addAddressNo = "1";
    } else if (this.showAddress2 == false) {
      this.showModalAdd()
      this.addAddressNo = "2";
    } else if (this.showAddress3 == false) {
      this.showModalAdd()
      this.addAddressNo = "3";
    } else {
      this.modalMessage = "ไม่สามารถเพิ่มที่อยู่ได้ท่านต้องมีที่อยู่ไม่เกิน 3 ในระบบ";
      this.modalResult = "มีข้อผิดพลาด !";
      this.modalIcon = "fas fa-times-circle iRed";
      this.colorH = "iRed";
      this.showModal();
    }
  }

  ngAfterViewInit() {
  }

  openModal() {
    this.display = 'block';
  }
  onCloseHandled() {
    this.display = 'none';
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  showModalAdd() {
    document.getElementById("btnModalAdd").click()
  }
  showConfirm() {
    document.getElementById("confirmModal").click()
  }
  showConfirmDelete(id) {
    this.deleteAddressNo = id;
    document.getElementById("DeleteModal").click()
  }


  checkModal() {
    if (this.statusModalCheck == 200) {
      this.router.navigate(['/index']);
    }
  }

}
