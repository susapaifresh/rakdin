
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ProductService } from '../../shared/service/product';
import { Router } from '@angular/router';
import { DropDown } from '../../shared/model/dropdown';
import { DropDownService } from '../../shared/service/dropdown';
import { productResult } from '../../shared/model/product';
import { MatPaginator, MatTableDataSource, MatSort, getMatIconFailedToSanitizeUrlError } from '@angular/material'

@Component({
  selector: 'app-edit-list-product',
  templateUrl: './edit-list-product.component.html',
  styleUrls: ['./edit-list-product.component.scss']
})
export class EditListProductComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;


  showLoader = false
  form: FormGroup;
  imageBanner = "";
  UnitDropDowns: DropDown[] = [];
  GroupDropDowns: DropDown[] = [];
  FarmerDropDowns: DropDown[] = [];

  allProduct: productResult[] = []

  displayedColumns: string[] = ['picture', 'id', 'productname', 'price', 'price2', 'group', 'unit', 'ownerfarmer', 'edit']
  dataSource = new MatTableDataSource<productResult>(this.allProduct);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  productname;
  productprice;
  productspecialprice;
  unit;
  picture;
  group;
  farmer;
  productid;
  productsearch;

  showproductforedit = false;
  constructor(private dropdownService: DropDownService, private productService: ProductService, private router: Router) {


  }

  search() {
    this.dataSource.filter = this.productsearch.trim().toLowerCase();
  }
  ngOnInit() {
    this.showLoader = true

    this.productService.getProductAll().subscribe(
      data => {
        this.showLoader = false
        console.log(data);
        this.dataSource = new MatTableDataSource<productResult>(data)
        this.dataSource.sort = this.sort
        this.dataSource.paginator = this.paginator
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }

  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    location.reload();
  }
}