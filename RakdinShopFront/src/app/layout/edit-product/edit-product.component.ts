
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ProductService } from '../../shared/service/product';
import { Router, ActivatedRoute } from '@angular/router';
import { DropDown } from '../../shared/model/dropdown';
import { DropDownService } from '../../shared/service/dropdown';
import { product } from '../../shared/model/product';
@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;


  showLoader = false
  form: FormGroup;
  imageBanner = "";
  UnitDropDowns: DropDown[] = [];
  GroupDropDowns: DropDown[] = [];
  FarmerDropDowns: DropDown[] = [];

  productAdd: product = new product;

  productname;
  productprice;
  productspecialprice;
  unit;
  picture;
  group;
  farmer;
  productid;
  inputpromotion;

  returnResultQty: boolean = false;
  isHideCtnMsg: boolean = true
  isHideCtnMsg2: boolean = true
  ctnmessage: string
  
  showproductforedit = false;
  constructor(private route: ActivatedRoute,private dropdownService: DropDownService, private productService: ProductService, private router: Router) {


  }
  check2() {
    this.returnResultQty = false

    let numberFormat = /^[0-9]\d*(\.\d+)?$/

    if (this.productspecialprice.match(numberFormat)) {
      if (parseFloat(this.productspecialprice) > 0) {
        this.returnResultQty = true
        this.isHideCtnMsg2 = true

      }
    }
    if (!this.returnResultQty) {
      this.isHideCtnMsg2 = false
      this.ctnmessage = "This field allows numbers to input only and can't be the zero value."
    }
  }
  check1() {
    this.returnResultQty = false
    let numberFormat = /^[0-9]\d*(\.\d+)?$/

    if (this.productprice.match(numberFormat)) {
      if (parseFloat(this.productprice) > 0) {
        this.returnResultQty = true
        this.isHideCtnMsg = true

      }
    }


    if (!this.returnResultQty) {
      this.isHideCtnMsg = false
      this.ctnmessage = "This field allows numbers to input only and can't be the zero value."
    }
  }
  ngOnInit() {
    this.showLoader = true;

    this.route.params.subscribe(params => {
      let id: string = params['id'];
      this.showLoader = true;

      this.productService.getProductDetail(id).subscribe(data => {
        this.showLoader = false
  
        this.showproductforedit = true;
  
        this.productid = data.id;
  
  
        this.productname = data.productname;
        this.productprice = data.price;
        this.productspecialprice = data.price2;
        this.farmer = data.ownerfarmerid;
        this.unit = data.unitid;
        this.group = data.groupid;
        this.picture = data.picture;
        this.inputpromotion = data.promotion
      },
        error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
    });

    this.dropdownService.getFarmerDropDown().subscribe(data => {
      this.FarmerDropDowns = data;

    },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
    this.dropdownService.getGroupDropDown().subscribe(data => {
      this.GroupDropDowns = data;

    },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
    this.dropdownService.getUnitDropDown().subscribe(data => {
      this.UnitDropDowns = data;

    },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  clearFill() {
    this.productname = null;
    this.productprice = null;
    this.productspecialprice = null;
    this.farmer = null;
    this.unit = null;
    this.group = null;
    this.picture = null;
  }
  search() {
    this.clearFill()

    this.showLoader = true;

    this.productService.getProductDetail(this.productid).subscribe(data => {
      this.showLoader = false

      this.showproductforedit = true;

      this.productid = data.id;


      this.productname = data.productname;
      this.productprice = data.price;
      this.productspecialprice = data.price2;
      this.farmer = data.ownerfarmerid;
      this.unit = data.unitid;
      this.group = data.groupid;
      this.picture = data.picture;
    },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  onRemoved(e) {
    this.imageBanner = '';
  }
  onUploadFinished(e) {
    this.imageBanner = e.src;
  }
  save() {
    this.showLoader = true
    if (this.imageBanner == "" || this.imageBanner == null) {
      this.productAdd.productname = this.productname;
      this.productAdd.price = this.productprice;
      this.productAdd.price2 = this.productspecialprice;
      this.productAdd.picture = this.picture;
      this.productAdd.unit = this.unit;
      this.productAdd.group = this.group;
      this.productAdd.ownerfarmer = this.farmer;
      this.productAdd.promotion = this.inputpromotion;

    } else {
      this.productAdd.productname = this.productname;
      this.productAdd.price = this.productprice;
      this.productAdd.price2 = this.productspecialprice;
      this.productAdd.picture = this.imageBanner;
      this.productAdd.unit = this.unit;
      this.productAdd.group = this.group;
      this.productAdd.ownerfarmer = this.farmer;
      this.productAdd.promotion = this.inputpromotion;

    }

    this.productService.updateProduct(this.productid,this.productAdd).subscribe(
      data => {
        this.showLoader = false
        this.modalIcon = "fas fa-check-circle iGreen"
        this.colorH = "iGreen"
        this.modalResult = "อัพเดทสินค้าสำเร็จ !"
        this.productname = "";
        this.productprice = null;
        this.productspecialprice = null;
        this.imageBanner = null;
        this.unit = null;
        this.group = null;
        this.farmer = null;
        this.showModal();
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  delete() {
    this.showLoader = true

    this.productService.deleteProduct(this.productid).subscribe(data => {
      this.showLoader = false
      this.modalIcon = "fas fa-check-circle iGreen"
      this.colorH = "iGreen"
      this.modalResult = "ลบสินค้าสำเร็จ !"
      this.productname = "";
      this.showModal();

    },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    this.router.navigate(['/editproductlist']);
  }
}