import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ReportService } from '../../shared/service/report';
import { DateFormatPipe, DateFormatPipe2 } from '../../shared/pipes/DateFormatPipe';
import { productOrderReport } from '../../shared/model/report';

export interface Status {
  value: string;
  viewValue: string;
}
export class CustomerTmp {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss'],
  animations: [routerTransition()]
})

export class ReportOrderComponent implements OnInit {
  length: number;
  dataSource: MatTableDataSource<productOrderReport>;
  fromdate: Date;
  todate: Date;
  clsPN: string;
  userInput: string;
  dataTmp: productOrderReport[];
  customerInput: string;
  statusInput: string;
  displayedColumns: string[] = ['เลขที่คำสั่งซื้อ', 'ผู้สั่งซื้อ', 'จำนวน', 'ราคา', 'วันที่สั่งซื้อ', 'สถานะ', 'รายละเอียด'];
  dataExport = [];
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;
  orderID ="";

  showLoader = false;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  customer: CustomerTmp[] = new Array;
  status: Status[] = [
    { value: 'ALL', viewValue: 'ALL' },
    { value: 'DONE', viewValue: 'DONE' },
    { value: 'PENDING', viewValue: 'PENDING' }
  ];
  constructor(private reportservice: ReportService) {
    this.fromdate = new Date();
    this.todate = new Date();
  }

  ngOnInit() {
    let defaultAllCustomer: CustomerTmp = new CustomerTmp;
    defaultAllCustomer.value = "ALL";
    defaultAllCustomer.viewValue = "ALL";
    this.customer.push(defaultAllCustomer);
  }

  search() {

    // this.reportquery.fromdate = new DateFormatPipe('en').transform(this.fromdate);
    // this.reportquery.todate = new DateFormatPipe('en').transform(this.todate);
    // this.reportquery.CLS_PN = this.checkparam(this.clsPN);
    // this.reportquery.OWNER_BY = this.checkparam(this.userInput);
    // this.reportquery.CUSTOMER_NAME = this.checkparam(this.customerInput);
    // this.reportquery.STATUS = this.checkparam(this.statusInput);

    this.showLoader = true

    //this.dataSource = new ReportDataSource(this.reportservice, this.reportqueryr);
    this.reportservice.getReport(new DateFormatPipe('en').transform(this.fromdate), new DateFormatPipe2('en').transform(this.todate),this.orderID.trim(),"").subscribe(
      data => {
        this.showLoader = false
        let reportSum: productOrderReport[] = [];
        data.forEach(dataorder => {
          let havevalue = false;

          for (let i = 0; i < reportSum.length; i++) {
            if (reportSum[i].id == dataorder.id) {
              havevalue = true;
              reportSum[i].quantity = reportSum[i].quantity + dataorder.quantity;
              reportSum[i].price = reportSum[i].price + (dataorder.quantity * dataorder.price);
            } else if (dataorder.id.indexOf) {
              havevalue = false;
            }
          }

          if (!havevalue) {
            let qtySum = dataorder.quantity;
            let priceSum = qtySum * dataorder.price;

            dataorder.status = this.setStatusToText(dataorder.status);
            dataorder.quantity = qtySum;
            dataorder.price = priceSum;
            reportSum.push(dataorder);
          }
        });

        this.dataSource = new MatTableDataSource<productOrderReport>(reportSum);
        this.dataSource.paginator = this.paginator;
        this.dataTmp = data;
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });

  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
  }
  setStatusToText(id) {
    switch (id) {
      case 0: {
        return 'ยังไม่ชำระ';
      }

      case 1: {
        return 'ยืนยันการชำระเงิน';
      }

      case 2: {
        return 'จัดส่งสินค้า';
      }
      case 3: {
        return 'ผู้สั่งได้รับสินค้าเรัยบร้อย';
      }

      default: {
        return 'ยังไม่ชำระ';
      }
    }
  }
}