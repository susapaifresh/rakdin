import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailCreateConfigComponent } from './email-create.component';

describe('UserComponent', () => {
  let component: EmailCreateConfigComponent;
  let fixture: ComponentFixture<EmailCreateConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailCreateConfigComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailCreateConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
