import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailEditService } from '../../shared/service/emailedit';
import { EmailConfigEmailListRole, CreateEmailConfig } from '../../shared/model/emailconfig';
import { FormsModule } from '@angular/forms';

export interface UserRole {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './email-create.component.html',
  styleUrls: ['./email-create.component.css'],
  animations: [routerTransition()],
})

export class EmailCreateConfigComponent implements OnInit {
  adminRole: EmailConfigEmailListRole;
  engineerRole: EmailConfigEmailListRole;
  technicianRole: EmailConfigEmailListRole;
  managerRole: EmailConfigEmailListRole;

  TEMPLATE_NAME;
  DESCRIPTION;
  SUBJECT;
  BODY;
  selectRole;
  blank: string | '';
  form: FormsModule;
  isAdmin: boolean | false;
  isEngineer: boolean | false;
  istechnician: boolean | false;
  ismanager: boolean | false;

  dataSourceTmp: CreateEmailConfig = new CreateEmailConfig;
  submitted: boolean = false;
  sendto;
  ccto;
  userrole: UserRole[] = [
    { value: 'ISC01', viewValue: 'ADMIN' },
    { value: 'ISC02', viewValue: 'ENGINEER' },
    { value: 'ISC03', viewValue: 'TECHNICIAN' },
    { value: 'ISC04', viewValue: 'MANAGER' }
  ];
  onSubmit() {
    this.dataSourceTmp.SEND_TO = this.sendto;
    this.dataSourceTmp.USER_ROLE = this.selected;
    this.dataSourceTmp.TEMPLATE_NAME = this.TEMPLATE_NAME;
    this.dataSourceTmp.BODY = this.BODY;
    this.dataSourceTmp.SUBJECT = this.SUBJECT;
    this.dataSourceTmp.DESCRIPTION = this.DESCRIPTION;
    this.dataSourceTmp.MAIL_CC = this.ccto;


    this.emailService.createEmailConfig(this.dataSourceTmp).subscribe(
      data => {
        this.router.navigate(['/email']);
      },
      error => {

      });
  }
  public selected = this.userrole[0].viewValue;

  constructor(public router: Router, public route: ActivatedRoute, private emailService: EmailEditService) {
  }

  setsendmail() {
    if (this.selected == 'ISC01') {
      this.sendto = this.adminRole.Emails.toString();
    } else if (this.selected == 'ISC02') {
      this.sendto = this.engineerRole.Emails.toString();
    } else if (this.selected == 'ISC03') {
      this.sendto = this.technicianRole.Emails.toString();
    } else if (this.selected == 'ISC04') {
      this.sendto = this.managerRole.Emails.toString();
    }
  }

  ngOnInit() {
    this.emailService.getEmailConfigEmailList("ADMIN").subscribe(
      data => {
        this.adminRole = data;
      },
      error => {

      });
    this.emailService.getEmailConfigEmailList("ENGINEER").subscribe(
      data => {
        this.engineerRole = data;
      },
      error => {

      });
    this.emailService.getEmailConfigEmailList("TECHNICIAN").subscribe(
      data => {
        this.technicianRole = data;
      },
      error => {

      });
    this.emailService.getEmailConfigEmailList("MANAGER").subscribe(
      data => {
        this.managerRole = data;
      },
      error => {

      });


    this.route.params.subscribe(params => {
      let id: number = params['id'];
      this.emailService.getEmailConfigByID(id).subscribe(
        data => {
          if (data.USER_ROLE == 'ISC01') {
            this.isAdmin = true;
            this.selected = this.userrole[0].value;
            this.sendto = this.adminRole.Emails.toString();
          }
          if (data.USER_ROLE == 'ISC02') {
            this.isEngineer = true;
            this.selected = this.userrole[1].value;
            this.sendto = this.engineerRole.Emails.toString();
          }
          if (data.USER_ROLE == 'ISC03') {
            this.istechnician = true;
            this.selected = this.userrole[2].value;
            this.sendto = this.technicianRole.Emails.toString();
          }
          if (data.USER_ROLE == 'ISC04') {
            this.ismanager = true;
            this.selected = this.userrole[3].value;
            this.sendto = this.managerRole.Emails.toString();
          }

        },
        error => {

        });
    });
  }
  cancel() {
    this.router.navigate(['/email']);
  }
}