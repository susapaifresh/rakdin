import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  showLoader: boolean;
  constructor( public router: Router) {

  }

  ngOnInit() {
    if (this.router.url === '/') {
      // console.log('Check user', this.glob.getuser());
      this.router.navigate(['/index']);
    }
  }

}
