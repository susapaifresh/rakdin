
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MenuAccess } from '../../shared/model/user';
import { CustomerService } from '../../shared/service/customer';
import { CustomerObj } from '../../shared/model/customer';
declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;
  statusModalCheck = 0;

  model: any = {};
  loading = false;
  returnUrl: string;
  values = '';
  display = 'none';
  listmenutmp: MenuAccess[];
  checkPasswordMatch = true;

  constructor(private route: ActivatedRoute,
    private router: Router, private dialog: MatDialog, private CustomerService: CustomerService) {

  }

  ngOnInit() {
    localStorage.clear();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  ngAfterViewInit() {
  }

  openModal() {
    this.display = 'block';
  }
  onCloseHandled() {
    this.display = 'none';
  }
  showError(error: string): void {
    // this.dialog.open(AlertDialogConfigComponent, {
    //   data: { errorMsg: error }, width: '250px'
    // });
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  checkModal() {
    if (this.statusModalCheck == 200) {
      this.router.navigate(['/index']);
    }
  }
  register() {

    this.CustomerService.register(this.model.email, this.model.inputPassword,
      this.model.name, this.model.tel
      , this.model.homenumber, this.model.subdistrict
      , this.model.district, this.model.province
      , this.model.postcode, this.model.phone).subscribe(
        data => {
          this.loading = false;
          // for (let i = 0; i < data.customerList.length; i++) {
          //   let tmp = new CustomerTmp();
          //   tmp.value = data.customerList[i];
          //   tmp.viewValue = data.customerList[i];
          //   this.customerFilter.push(tmp);
          // }
          if (data.status != 200) {
            this.modalMessage = "Email นี้มีอยู่ในระบบแล้วโปรดลองอีกครั้ง";
            this.modalResult = "มีข้อผิดพลาด !";
            this.modalIcon = "fas fa-times-circle iRed";
            this.colorH = "iRed";
            this.statusModalCheck = 400;
          } else {
            this.statusModalCheck = 200;
            let obj: CustomerObj = JSON.parse(data.obj);
            // save user
            localStorage.setItem("nameuser", obj.nameuser);
            localStorage.setItem("email", obj.email);
            //save access menu
            localStorage.setItem("id", JSON.stringify(obj.id));
            //save access role
            localStorage.setItem("role", "0");

            this.modalMessage = "";
            this.modalResult = "ลงทะเบียนสำเร็จ";
            this.modalIcon = "fas fa-check-circle iGreen";
            this.colorH = "iGreen";
          }
          this.showModal();

        },
        error => {
          this.statusModalCheck = 400;

          this.loading = false;
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
  }
}
