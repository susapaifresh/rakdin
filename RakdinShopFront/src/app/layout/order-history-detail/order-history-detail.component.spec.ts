import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderHistoryDetailReportComponent } from './order-history-detail.component';

describe('OrderHistoryDetailReportComponent', () => {
  let component: OrderHistoryDetailReportComponent;
  let fixture: ComponentFixture<OrderHistoryDetailReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderHistoryDetailReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderHistoryDetailReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
