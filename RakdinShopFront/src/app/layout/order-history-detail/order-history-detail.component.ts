
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { productOrderReport } from '../../shared/model/report';
import { ReportService } from '../../shared/service/report';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { CustomerService } from '../../shared/service/customer';

@Component({
  selector: 'app-order-history-detail',
  templateUrl: './order-history-detail.component.html',
  styleUrls: ['./order-history-detail.component.scss']
})
export class OrderHistoryDetailReportComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  showLoader = false;
  displayedColumns: string[] = ['เลขที่คำสั่งซื้อ', 'สินค้า', 'จำนวน', 'ราคา', 'ราคารวม', 'วันที่สั่งซื้อ'];
  dataSource: MatTableDataSource<productOrderReport>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  orderid = '';
  sumprice = 0;
  nameuser;
  address;
  statusDropDowns: DropdownSector[] = [];
  statusOrder;

  tmpChangeDropdown;

  constructor(private route: ActivatedRoute, private reportservice: ReportService, private customerService: CustomerService) {
  }


  ngOnInit() {
    let north = new DropdownSector();
    north.id = 0;
    north.name = "ยังไม่ชำระ";

    let t = new DropdownSector();
    t.id = 1;
    t.name = "ยินยันการชำระเงิน";

    let m = new DropdownSector();
    m.id = 2;
    m.name = "จัดส่งสินค้า";

    let s = new DropdownSector();
    s.id = 3;
    s.name = "ผู้สั่งได้รับสินค้าเรัยบร้อย";

    this.statusDropDowns.push(north);
    this.statusDropDowns.push(t);
    this.statusDropDowns.push(m);
    this.statusDropDowns.push(s);

    this.showLoader = true;

    this.route.params.subscribe(params => {
      let id: string = params['id'];
      this.sumprice = 0;
      this.reportservice.getReportDetail(id).subscribe(
        data => {
          for (let i = 0; i < data.length; i++) {
            this.sumprice = this.sumprice + (data[i].price * data[i].quantity);
          }
          this.orderid = data[0].id
          this.nameuser = data[0].nameuser
          this.statusOrder = data[0].status
          this.tmpChangeDropdown = this.statusOrder;
          this.showLoader = false;
          this.dataSource = new MatTableDataSource<productOrderReport>(data);
          this.dataSource.paginator = this.paginator;
          this.address = data[0].address;

        },
        error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
    });
  }
  setStatusToText(id) {
    switch (id) {
      case 0: {
        return 'ยังไม่ชำระ';
      }

      case 1: {
        return 'ยืนยันการชำระเงิน';
      }

      case 2: {
        return 'จัดส่งสินค้า';
      }
      case 3: {
        return 'ผู้สั่งได้รับสินค้าเรัยบร้อย';
      }

      default: {
        return 'ยังไม่ชำระ';
      }
    }
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()

  }
}
export class DropdownSector {
  id: number;
  name: string;
}