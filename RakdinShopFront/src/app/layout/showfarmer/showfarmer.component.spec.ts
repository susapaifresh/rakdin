import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowFarmerComponent } from './showfarmer.component';

describe('LoginComponent', () => {
  let component: ShowFarmerComponent;
  let fixture: ComponentFixture<ShowFarmerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShowFarmerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowFarmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
