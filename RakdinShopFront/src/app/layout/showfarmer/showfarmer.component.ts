
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FarmerService } from '../../shared/service/farmer';
import { Farmer, FarmerResult } from '../../shared/model/farmer';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-showfarmer',
  templateUrl: './showfarmer.component.html',
  styleUrls: ['./showfarmer.component.scss']
})
export class ShowFarmerComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  dataFarmers: Farmer[] = [];
  showLoader = false
  test = '';
  constructor(private farmerservice: FarmerService, private router: Router) {

  }

  ngOnInit() {
    this.showLoader = true

    this.farmerservice.getAllFarmer().subscribe(
      data => {
        this.showLoader = false
        //this.test = data[0].detail;
        // this.test = data[0].imageprofile;
        this.dataFarmers = data;
        console.log(data);
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }

  showDetail(id){
    this.router.navigate(['/showfarmer/'+id]);
  }
  buildArr(theArr: Farmer[]): Farmer[][] {

    var arrOfarr = [];
    for (var i = 0; i < theArr.length; i += 3) {
      var row = [];

      for (var x = 0; x < 3; x++) {
        var value = theArr[i + x];
        if (!value) {
          break;
        }
        row.push(value);
      }
      arrOfarr.push(row);
    }
    return arrOfarr;
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    this.router.navigate(['/addframer']);

  }
}