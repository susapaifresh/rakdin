import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../shared/service/User';
import { MatDialog } from '@angular/material';
import { MenuAccess } from '../../shared/model/user';
import { CustomerObj } from '../../shared/model/customer';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  model: any = {};
  showLoader = false;
  returnUrl: string;
  values = '';
  listmenutmp: MenuAccess[];

  constructor(private route: ActivatedRoute,
    private router: Router, public user: UserService, private dialog: MatDialog) { }


  ngOnInit() {
    // reset login status
    //localStorage.clear();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  showError(error: string): void {
    // this.dialog.open(AlertDialogConfigComponent, {
    //   data: { errorMsg: error }, width: '250px'
    // });
  }
  showErrorNotAuthorize(error: string): void {
    // this.dialog.open(AlertDialogNotAuthorizeConfigComponent, {
    //   data: { errorMsg: error }, width: '250px'
    // });
  }
  login() {
    this.showLoader = true;

    this.user.login(this.model.username, this.model.password)
      .subscribe(
        data => {
          this.showLoader = false;
          if (data.status == 200) {
            let obj: CustomerObj = JSON.parse(data.obj);
            // save user
            // save user
            localStorage.setItem("nameuser", obj.nameuser);
            localStorage.setItem("email", obj.email);
            //save access menu
            localStorage.setItem("id", obj.id.toString());
            //save access role
            localStorage.setItem("role", obj.role.toString());

            this.router.navigateByUrl('/login', { skipLocationChange: true }).then(() =>
              this.router.navigate(['/index']));

          } else {
            this.showLoader = false
            this.modalMessage = "Please check your username and password.";
            this.modalResult = "Failed !";
            this.modalIcon = "fas fa-times-circle iRed";
            this.colorH = "iRed";
            this.showModal();
          }
        }, error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
          console.log(error);
        });
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()

  }
}
