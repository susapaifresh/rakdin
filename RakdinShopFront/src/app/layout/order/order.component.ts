
import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../shared/service/customer';
import { Router } from '@angular/router';
import { productOrder } from 'src/app/shared/model/cart';
import { CustomerObj } from 'src/app/shared/model/customer';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  address;
  showLoader = false
  itemIncartObj = [];
  sumprice = 0;

  datatmp: CustomerObj = new CustomerObj();

  hidden1 = false;
  hidden2 = false;
  hidden3 = false;

  notHaveAddress = false;
  constructor(private customerService: CustomerService, private router: Router) {


  }
  setAddress(id) {
    console.log();
    if (id == 1) {
      this.address = 'ชื่อผู้รับ ' + this.datatmp.nameuser + ' โทร. ' + this.datatmp.phone + ' มือถือ. ' + this.datatmp.tel +
        '\n' + this.datatmp.homenumber
        + ' ตำบล ' + this.datatmp.subdistrict
        + ' อำเภอ ' + this.datatmp.district
        + ' จังหวัด ' + this.datatmp.province
        + ' รหัสไปรษณีย์ ' + this.datatmp.postcode
    }
    if (id == 2) {
      this.address = 'ชื่อผู้รับ ' + this.datatmp.nameuser + ' โทร. ' + this.datatmp.phone + ' มือถือ. ' + this.datatmp.tel +
        '\n' + this.datatmp.homenumber2
        + ' ตำบล ' + this.datatmp.subdistrict2
        + ' อำเภอ ' + this.datatmp.district2
        + ' จังหวัด ' + this.datatmp.province2
        + ' รหัสไปรษณีย์ ' + this.datatmp.postcode2
    }
    if (id == 3) {
      this.address = 'ชื่อผู้รับ ' + this.datatmp.nameuser + ' โทร. ' + this.datatmp.phone + ' มือถือ. ' + this.datatmp.tel +
        '\n' + this.datatmp.homenumber3
        + ' ตำบล ' + this.datatmp.subdistrict3
        + ' อำเภอ ' + this.datatmp.district3
        + ' จังหวัด ' + this.datatmp.province3
        + ' รหัสไปรษณีย์ ' + this.datatmp.postcode3
    }
  }
  ngOnInit() {
    this.generadeOrderId()
    this.customerService.getCustomerDetail(parseInt(localStorage.getItem('id'))).subscribe(
      data => {
        this.showLoader = false
        this.datatmp = data;


        if (data.homenumber == "-") {
          this.hidden1 = true;
        }
        if (data.homenumber2 == "-") {
          this.hidden2 = true;
        }
        if (data.homenumber3 == "-") {
          this.hidden3 = true;
        }

        if (!this.hidden1) {
          this.address = 'ชื่อผู้รับ ' + data.nameuser + ' โทร. ' + data.phone + ' มือถือ. ' + this.datatmp.tel +
            '\n' + data.homenumber
            + ' ตำบล ' + data.subdistrict
            + ' อำเภอ ' + data.district
            + ' จังหวัด ' + data.province
            + ' รหัสไปรษณีย์ ' + data.postcode
        } else if (!this.hidden2) {
          this.address = 'ชื่อผู้รับ ' + data.nameuser + ' โทร. ' + data.phone + ' มือถือ. ' + this.datatmp.tel +
            '\n' + data.homenumber2
            + ' ตำบล ' + data.subdistrict2
            + ' อำเภอ ' + data.district2
            + ' จังหวัด ' + data.province2
            + ' รหัสไปรษณีย์ ' + data.postcode2
        } else if (!this.hidden3) {
          this.address = 'ชื่อผู้รับ ' + data.nameuser + ' โทร. ' + data.phone + ' มือถือ. ' + this.datatmp.tel +
            '\n' + data.homenumber3
            + ' ตำบล ' + data.subdistrict3
            + ' อำเภอ ' + data.district3
            + ' จังหวัด ' + data.province3
            + ' รหัสไปรษณีย์ ' + data.postcode3
        } else {
          this.address = "กรุณาเพิ่มที่อยู่ก่อนสั่งซื้อสินค้า"
          this.notHaveAddress = true;
        }
        let cartAll = JSON.parse(localStorage.getItem('cart'));
        this.itemIncartObj = cartAll;
        this.sumpriceorder();
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });

  }
  deleteItemInCart(id) {
    let tmp = [];
    for (let i = 0; i < this.itemIncartObj.length; i++) {
      if (id != this.itemIncartObj[i].id) {
        tmp.push(this.itemIncartObj[i]);
      }
    }
    if (tmp == null) {
      localStorage.removeItem('cart');
    } else {
      this.itemIncartObj = tmp;
      localStorage.removeItem('cart');
      localStorage.setItem('cart', JSON.stringify(tmp));
    }

    const el: HTMLElement = document.getElementById('cartcontrol');
    el.click();
    this.sumpriceorder();
  }

  generadeOrderId() {
    let today = new Date();
    let tmp = formatDate(today, 'yyddMMhhmmss', 'en-US', '+07').toString();
    tmp += localStorage.getItem('id')
    return tmp;
  }
  ordernow() {
    // let orderid = Date.now() + parseInt(localStorage.getItem('id'));
    let orderid = this.generadeOrderId();


    let ordertmp: productOrder[] = [];
    for (let i = 0; i < this.itemIncartObj.length; i++) {
      let tmporeder = new productOrder();
      tmporeder.productname = this.itemIncartObj[i].productname;
      tmporeder.price = this.itemIncartObj[i].price;
      tmporeder.group = this.itemIncartObj[i].group;
      tmporeder.quantity = this.itemIncartObj[i].quantity;
      tmporeder.idproduct = this.itemIncartObj[i].id;
      tmporeder.unit = this.itemIncartObj[i].unit;
      tmporeder.id = orderid;
      tmporeder.address = this.address;
      tmporeder.userid = parseInt(localStorage.getItem('id'));
      ordertmp.push(tmporeder);
    }

    this.showLoader = true

    this.customerService.orderProduct(ordertmp).subscribe(
      data => {
        if (!this.notHaveAddress) {
          if (data['status'] == 200) {
            this.showLoader = false
            this.modalIcon = "fas fa-check-circle iGreen"
            this.colorH = "iGreen"
            this.modalResult = "สำเร็จ !"
            this.modalMessage = "ท่านทำการสั่งซื้อสินค้าสำเร็จแล้วสามารถตรวจสอบได้ที่รายการสั่งซื้อของฉัน";
            localStorage.removeItem('cart');
            const el: HTMLElement = document.getElementById('cartcontrol');
            el.click();
          } else {
            this.showLoader = false
            this.modalMessage = "ท่านทำการสั่งซื้อสินค้าไม่สำเร็จกรุณาลองใหม่อีกครั้ง";
            this.modalResult = "มีข้อผิดพลาด !";
            this.modalIcon = "fas fa-times-circle iRed";
            this.colorH = "iRed";
          }
        } else {
          this.showLoader = false
          this.modalMessage = "โปรดระบุที่อยู่";
          this.modalResult = "มีข้อผิดพลาด !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
        }


        this.showModal();

      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });

  }
  sumpriceorder() {
    this.sumprice = 0;
    for (let i = 0; i < this.itemIncartObj.length; i++) {
      this.sumprice = this.sumprice + (this.itemIncartObj[i].price * this.itemIncartObj[i].quantity);
    }
  }
  changeQty(item, value) {
    for (let i = 0; i < this.itemIncartObj.length; i++) {
      if (item.id == this.itemIncartObj[i].id) {
        this.itemIncartObj[i].quantity = value.target.value;
      }
    }
    localStorage.removeItem('cart');
    localStorage.setItem('cart', JSON.stringify(this.itemIncartObj));
    this.sumpriceorder();
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    this.router.navigate(['/index']);

  }
}