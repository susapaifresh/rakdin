
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { MenuAccess } from '../../shared/model/user';
import { CustomerService } from '../../shared/service/customer';
import { CustomerObj } from '../../shared/model/customer';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.css']
})
export class EditPasswordComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  email;
  id;

  model: any = {};
  loading = false;
  returnUrl: string;
  values = '';
  display = 'none';
  listmenutmp: MenuAccess[];
  checkPasswordMatch = false;
  constructor(private route: ActivatedRoute,
    private router: Router, private dialog: MatDialog, private CustomerService: CustomerService) { }


  ngOnInit() {
    this.route.params.subscribe(params => {
      let id: number = params['id'];
      this.loading = true;

      this.CustomerService.checkUrl(id).subscribe(data => {
        this.loading = false;

        this.email = data.email;
        this.id = data.id;
      }, error => {
        this.loading = false;
        this.modalMessage = "โปรดตวจสอบ";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      })
    });
  }

  checkRePassword() {
    if (this.model.inputRePassword == this.model.inputPassword) {
      this.checkPasswordMatch = true;
    } else {
      this.checkPasswordMatch = false;
    }
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    this.router.navigate(['/index']);
  }
  savenewpassword() {
    this.loading = true;

    this.CustomerService.changePassword(this.id, this.model.inputPassword).subscribe(data => {
      this.loading = false;
      this.modalMessage = "เปลี่ยนรหัสผ่านสำเร็จ";
      this.modalResult = "สำเร็จ";
      this.modalIcon = "fas fa-check-circle iGreen"
      this.colorH = "iGreen"
      this.showModal();
    }, error => {
      this.loading = false;
      this.modalMessage = "โปรดตวจสอบ";
      this.modalResult = "Failed !";
      this.modalIcon = "fas fa-times-circle iRed";
      this.colorH = "iRed";
      this.showModal();
    })
  }
}
