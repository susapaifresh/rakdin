
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FarmerService } from '../../shared/service/farmer';
import { Farmer, FarmerResult } from '../../shared/model/farmer';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-farmerdetail',
  templateUrl: './farmerdetail.component.html',
  styleUrls: ['./farmerdetail.component.scss']
})
export class FarmerDetailComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  dataFarmer: Farmer = new Farmer;
  
  detailFarmer;
  showLoader = false
  test = '';
  constructor(private route: ActivatedRoute, private farmerservice: FarmerService, private router: Router) {

  }

  ngOnInit() {
    this.showLoader = true
    this.route.params.subscribe(params => {
      let id: string = params['id'];
      this.farmerservice.getFarmer(id).subscribe(
        data => {
          this.showLoader = false
          this.dataFarmer = data;
          this.detailFarmer = data.detail;
          console.log(data);
        },
        error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
    });
  }
  
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    this.router.navigate(['/addframer']);

  }
}