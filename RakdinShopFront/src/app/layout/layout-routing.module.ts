import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { LayoutComponent } from './layout.component';
import { DashBoardComponent } from './dashboard/dashboard.component';
import { EmailEditConfigComponent } from './email-edit/email-edit.component';
import { EmailCreateConfigComponent } from './email-create/email-create.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import { AdminGuard } from '../auth/guards/admin.guard';
import { RegisterComponent } from './register/register.component';
import { AddFarmerComponent } from './add-farmer/add-farmer.component';
import { ShowFarmerComponent } from './showfarmer/showfarmer.component';
import { FarmerDetailComponent } from './farmerdetail/farmerdetail.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';
import { AddProductComponent } from './add-product/add-product.component';
import { OrderComponent } from './order/order.component';
import { ReportOrderComponent } from './reportorder/report.component';
import { LoginComponent } from './login/login.component';
import { ProductComponent } from './product/product.component';
import { OrderDetailReportComponent } from './order-detail-report/order-detail.component';
import { UploadReceiptComponent } from './upload-receipt/upload-receipt.component';
import { ReportReceiptComponent } from './report-receipt/report-receipt.component';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderHistoryDetailReportComponent } from './order-history-detail/order-history-detail.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditPasswordComponent } from './edit-password/edit-password.component';
import { EditListProductComponent } from './edit-list-product/edit-list-product.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AddFirstPageComponent } from './add-firstpage/add-firstpage.component';


const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'user', component: UserComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'addfarmer', component: AddFarmerComponent, canActivate: [AdminGuard] },
      { path: 'productdetail/:id', component: ProductDetailComponent },
      { path: 'addproduct', component: AddProductComponent, canActivate: [AdminGuard] },
      { path: 'editproduct/:id', component: EditProductComponent, canActivate: [AdminGuard] },
      { path: 'editproductlist', component: EditListProductComponent, canActivate: [AdminGuard] },
      { path: 'editproductlist', component: EditListProductComponent, canActivate: [AdminGuard] },
      { path: 'addfirstpage', component: AddFirstPageComponent, canActivate: [AdminGuard] },

      
      // { path: 'reportedit', component: ReportEditComponent, canActivate: [AuthGuard] },
      { path: 'index', component: DashBoardComponent },
      { path: 'emailedit/:id', component: EmailEditConfigComponent },
      { path: 'emailcreate', component: EmailCreateConfigComponent },
      { path: 'editpassword/:id', component: EditPasswordComponent },
      { path: 'showfarmer', component: ShowFarmerComponent },
      { path: 'showfarmer/:id', component: FarmerDetailComponent },
      { path: 'order', component: OrderComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'reportorder', component: ReportOrderComponent, canActivate: [AdminGuard] },
      { path: 'reportorderdetail/:id', component: OrderDetailReportComponent, canActivate: [AdminGuard] },
      { path: 'product', component: ProductComponent },
      { path: 'uploadreceipt', component: UploadReceiptComponent, canActivate: [AuthGuard] },
      { path: 'reportreceipt', component: ReportReceiptComponent, canActivate: [AdminGuard] },
      { path: 'orderhistory', component: OrderHistoryComponent, canActivate: [AuthGuard] },
      { path: 'orderhistorydetail/:id', component: OrderHistoryDetailReportComponent, canActivate: [AuthGuard] },
      { path: 'editprofile', component: EditProfileComponent, canActivate: [AuthGuard] },
      
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class LayoutRoutingModule { }