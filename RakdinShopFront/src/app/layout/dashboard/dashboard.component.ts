import { Component, OnInit } from '@angular/core';
import { productResult } from '../../shared/model/product';
import { ProductService } from '../../shared/service/product';
import { FirstPageService } from '../../shared/service/firstpage';
import { productCart } from '../../shared/model/cart';

@Component({
  selector: 'app-user',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashBoardComponent implements OnInit {

  imageUrls: (string | IImage)[];

  height: string = '400px';
  minHeight: string;
  arrowSize: string = '30px';
  showArrows: boolean = true;
  disableSwiping: boolean = false;
  autoPlay: boolean = true;
  autoPlayInterval: number = 3333;
  stopAutoPlayOnSlide: boolean = true;
  debug: boolean = false;
  backgroundSize: string = 'cover';
  backgroundPosition: string = 'center center';
  backgroundRepeat: string = 'no-repeat';
  showDots: boolean = true;
  dotColor: string = '#FFF';
  showCaptions: boolean = true;
  captionColor: string = '#FFF';
  captionBackground: string = 'rgba(0, 0, 0, .35)';
  lazyLoad: boolean = false;
  hideOnNoSlides: boolean = false;
  width: string = '100%';
  dataProduct: productResult[] = []
  dataProductshow: productResult[] = []
  addProductToCart: productCart[] = [];

  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;
  detailaboutUs;

  constructor(private productService: ProductService, private firstPageService: FirstPageService) { }

  ngOnInit() {
    // adding an image url dynamically.
    // setTimeout(() => {
    //   console.log('adding an image url dynamically.');
    //   this.imageUrls.push('https://cdn-images-1.medium.com/max/2000/1*Nccd2ofdArlXF7v58UK94Q.jpeg');
    // }, 2000);
    const el: HTMLElement = document.getElementById('updateheader');
    el.click();
    this.firstPageService.getFirstPage().subscribe(
      data => {
        this.imageUrls = [
          { url: data.imageBanner1 },
          { url: data.imageBanner2 },
          { url: data.imageBanner3 },
          { url: data.imageBanner4 }
        ];
      this.detailaboutUs =  data.aboutUs;
      },
      error => {
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });

    this.productService.getProductAll().subscribe(
      data => {
        this.dataProduct = data;
        this.dataProductshow.push(data[0]);
        this.dataProductshow.push(data[1]);
        this.dataProductshow.push(data[2]);
        //this.dataProductshow.push(data[3]);

      },
      error => {
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
  }
  addProduct(dataProduct) {
    let tmpAddCart = new productCart();

    tmpAddCart.id = dataProduct.id;
    tmpAddCart.productname = dataProduct.productname;
    tmpAddCart.price = dataProduct.price;
    tmpAddCart.group = dataProduct.group;
    tmpAddCart.picture = dataProduct.picture;
    tmpAddCart.unit = dataProduct.unit;
    tmpAddCart.quantity = 1;
    this.addProductToCart = [];
    this.addProductToCart.push(tmpAddCart);
    //localStorage.setItem('cart', JSON.stringify([this.addProductToCart]));
    
    let animation = document.getElementById('e'+dataProduct.id);

    animation.classList.remove('img-tmp');
    animation.style.animation = 'none';
    animation.offsetHeight; /* trigger reflow */
    animation.style.animation = null;
    animation.classList.add('img-tmp');

    const el: HTMLElement = document.getElementById('cartAnimation');
    el.click();

    this.checkProductInCart();
  }

  checkProductInCart() {
    let haveInCart = false;
    let cartAll = JSON.parse(localStorage.getItem('cart'));
    //check have item in cart

    if (cartAll == null) {
      localStorage.setItem('cart', JSON.stringify(this.addProductToCart));
    } else {
      for (let i = 0; i < cartAll.length; i++) {
        if (this.addProductToCart[0].id == cartAll[i].id) {
          cartAll[i].quantity = cartAll[i].quantity + this.addProductToCart[0].quantity;
          haveInCart = true;
        }
      }
      if (!haveInCart) {
        cartAll[cartAll.length] = this.addProductToCart[0];

        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(cartAll));
      } else {
        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(cartAll));
      }
    }
    const el: HTMLElement = document.getElementById('cartcontrol');
    el.click();
  }
}
export interface IImage {
  url: string | null;
  href?: string;
  clickAction?: Function;
  caption?: string;
  title?: string;
  backgroundSize?: string;
  backgroundPosition?: string;
  backgroundRepeat?: string;
}

