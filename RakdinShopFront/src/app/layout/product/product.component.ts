
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../shared/service/product';
import { productResult } from '../../shared/model/product';
import { Router, ActivatedRoute } from '@angular/router';
import { DropDown } from 'src/app/shared/model/dropdown';
import { productCart } from '../../shared/model/cart';
import { DropDownService } from '../../shared/service/dropdown';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  search = '';
  dataProduct: productResult[] = []
  dataProductTmpFillter: productResult[] = []
  dataProductTmpTap: productResult[] = []

  currentTap = 'all'

  groupTab: DropDown[] = []

  showLoader = false
  addProductToCart: productCart[] = [];
  checkHaveProduct = false;
  constructor(private groupService: DropDownService, private router: Router, private productService: ProductService) {
    groupService.getGroupDropDown().subscribe(
      data => {
        this.groupTab = data;
      }
      , error => {

      });
  }

  ngOnInit() {
    this.showLoader = true

    this.productService.getProductAll().subscribe(
      data => {
        this.showLoader = false
        this.checkHaveProduct = true;

        this.dataProduct = data;
        this.dataProductTmpTap = data;
        this.checkHaveProductFunction();
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  searchProduct() {
    if (this.search.trim() != "" && this.search != null) {
      this.dataProductTmpFillter = [];
      for (let i = 0; i < this.dataProductTmpTap.length; i++) {
        if (this.dataProductTmpTap[i].productname.includes(this.search)) {
          this.dataProductTmpFillter.push(this.dataProductTmpTap[i]);
        }
      }
      this.dataProduct = this.dataProductTmpFillter;

      this.checkHaveProductFunction();
    } else {
      this.dataProduct = this.dataProductTmpTap;
    }

  }
  checkHaveProductFunction() {
    if (this.dataProduct.length > 0) {
      this.checkHaveProduct = true;
    } else {

      this.checkHaveProduct = false;
    }
  }
  changeData(tab) {
    this.search = '';
    this.currentTap = tab;
    this.showLoader = true;
    this.checkHaveProduct = true;
    this.dataProduct = [];
    if (tab == 'all') {
      this.productService.getProductAll().subscribe(
        data => {
          this.showLoader = false
          //this.test = data[0].detail;
          // this.test = data[0].imageprofile;
          this.dataProduct = data;
          this.dataProductTmpTap = this.dataProduct;

          this.checkHaveProductFunction();
        },
        error => {
          this.dataProduct = [];

          this.checkHaveProductFunction();
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
    } else {
      this.productService.getProductByGroupAll(tab).subscribe(
        data => {
          this.showLoader = false
          this.dataProduct = data;
          this.dataProductTmpTap = this.dataProduct;

          this.checkHaveProductFunction();
        },
        error => {
          this.dataProduct = [];

          this.checkHaveProductFunction();
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        });
    }
  }
  showDetail(id) {
    this.router.navigate(['/showfarmer/' + id]);
  }
  buildArr(theArr: productResult[]): productResult[][] {

    var arrOfarr = [];
    for (var i = 0; i < theArr.length; i += 4) {
      var row = [];

      for (var x = 0; x < 4; x++) {
        var value = theArr[i + x];
        if (!value) {
          break;
        }
        row.push(value);
      }
      arrOfarr.push(row);
    }
    return arrOfarr;
  }
  addProduct(dataProduct) {
    let tmpAddCart = new productCart();

    tmpAddCart.id = dataProduct.id;
    tmpAddCart.productname = dataProduct.productname;
    tmpAddCart.price = dataProduct.price;
    tmpAddCart.group = dataProduct.group;
    tmpAddCart.picture = dataProduct.picture;
    tmpAddCart.unit = dataProduct.unit;
    tmpAddCart.quantity = 1;
    this.addProductToCart = [];
    this.addProductToCart.push(tmpAddCart);
    //localStorage.setItem('cart', JSON.stringify([this.addProductToCart]));

    
    let animation = document.getElementById('e'+dataProduct.id);
    animation.classList.remove('img-tmp');
    animation.style.animation = 'none';
    animation.offsetHeight; /* trigger reflow */
    animation.style.animation = null;

    animation.classList.add('img-tmp');
    const el: HTMLElement = document.getElementById('cartAnimation');
    el.click();
    this.checkProductInCart();
  }

  checkProductInCart() {
    let haveInCart = false;
    let cartAll = JSON.parse(localStorage.getItem('cart'));
    //check have item in cart

    if (cartAll == null) {
      localStorage.setItem('cart', JSON.stringify(this.addProductToCart));
    } else {
      for (let i = 0; i < cartAll.length; i++) {
        if (this.addProductToCart[0].id == cartAll[i].id) {
          cartAll[i].quantity = cartAll[i].quantity + this.addProductToCart[0].quantity;
          haveInCart = true;
        }
      }
      if (!haveInCart) {
        cartAll[cartAll.length] = this.addProductToCart[0];

        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(cartAll));
      } else {
        localStorage.removeItem('cart');
        localStorage.setItem('cart', JSON.stringify(cartAll));
      }
    }

    const el: HTMLElement = document.getElementById('cartcontrol');
    el.click();
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
  }
}