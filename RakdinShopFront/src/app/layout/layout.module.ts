import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { UserComponent } from './user/user.component';
import { ReportOrderComponent } from './reportorder/report.component';
import { DashBoardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { EmailEditConfigComponent } from './email-edit/email-edit.component';
import { AddProductComponent } from './add-product/add-product.component';
import { EditPasswordComponent } from './edit-password/edit-password.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditListProductComponent } from './edit-list-product/edit-list-product.component';
import { EmailCreateConfigComponent } from './email-create/email-create.component';
import { ShowFarmerComponent } from './showfarmer/showfarmer.component';
import { FarmerDetailComponent } from './farmerdetail/farmerdetail.component';
import { HeaderComponent, FooterComponent } from '../shared/components';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StorageServiceModule } from 'angular-webstorage-service';
import { ReportService } from '../shared/service/report';
import { FarmerService } from '../shared/service/farmer';
import { ProductService } from '../shared/service/product';
import { DropDownService } from '../shared/service/dropdown';
import { EmailEditService } from '../shared/service/emailedit';
import { FirstPageService } from '../shared/service/firstpage';
import { ReceiptService } from '../shared/service/receipt';
import { DateFormatPipe, DateFormatPipe2, DateFormatPipe3, DateFormatPipe4 } from '../shared/pipes/DateFormatPipe';
import { MatFormFieldModule } from '@angular/material';
import { ConfirmationDialogService } from '../shared/components/confirmation-dialog/confirmation-dialog.service';
import { ConfirmationDialogComponent } from '../shared/components/confirmation-dialog/confirmation-dialog.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SlideshowModule } from 'ng-simple-slideshow';
import { RegisterComponent } from './register/register.component';
import { ProductDetailComponent } from './productdetail/productdetail.component';
import { OrderComponent } from './order/order.component';
import { ProductComponent } from './product/product.component';
import { UploadReceiptComponent } from './upload-receipt/upload-receipt.component';
import { AddFarmerComponent } from './add-farmer/add-farmer.component';
import { ReportReceiptComponent } from './report-receipt/report-receipt.component';
import { QuillModule } from 'ngx-quill'
import { ImageUploadModule } from "angular2-image-upload";
import { NumberOnlyDirective } from '../shared/model/numberonly';
import { OrderDetailReportComponent } from './order-detail-report/order-detail.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { OrderHistoryComponent } from './order-history/order-history.component';
import { OrderHistoryDetailReportComponent } from './order-history-detail/order-history-detail.component';
import { NgxPaginationModule } from 'ngx-pagination';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AddFirstPageComponent } from './add-firstpage/add-firstpage.component';

@NgModule({
  declarations: [
    NumberOnlyDirective,
    ShowFarmerComponent,
    OrderHistoryComponent,
    OrderHistoryDetailReportComponent,
    OrderDetailReportComponent,
    RegisterComponent,
    EditProfileComponent,
    LayoutComponent,
    UserComponent,
    ReportOrderComponent,
    DashBoardComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    FarmerDetailComponent,
    EmailEditConfigComponent,
    AddProductComponent,
    AddFirstPageComponent,
    EditPasswordComponent,
    EditProductComponent,
    EditListProductComponent,
    EmailCreateConfigComponent,
    DateFormatPipe,
    DateFormatPipe2,
    DateFormatPipe3,
    DateFormatPipe4,
    ConfirmationDialogComponent,
    AddFarmerComponent,
    ProductDetailComponent,
    OrderComponent,
    ProductComponent,
    UploadReceiptComponent,
    ReportReceiptComponent
  ],
  imports: [
    NgxPaginationModule,
    NgxMaterialTimepickerModule.forRoot(),
    ImageUploadModule.forRoot(),
    NgbModule.forRoot(),
    SlideshowModule,
    QuillModule,
    MatFormFieldModule,
    StorageServiceModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    LayoutRoutingModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    CommonModule
  ],
  entryComponents: [ConfirmationDialogComponent],
  providers: [FirstPageService,DropDownService, ProductService, FarmerService, ConfirmationDialogService, ReportService, EmailEditService, ReceiptService]
})

export class LayoutModule { }
