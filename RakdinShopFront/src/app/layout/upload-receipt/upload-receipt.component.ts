
import { Component, OnInit } from '@angular/core';
import { ReceiptService } from '../../shared/service/receipt';
import { Router } from '@angular/router';
import { ReceiptModel } from '../../shared/model/receipt';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { DateFormatPipe3 } from '../../shared/pipes/DateFormatPipe';
import { UserService } from 'src/app/shared/service/User';
import { CustomerService } from 'src/app/shared/service/customer';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-upload-receipt',
  templateUrl: './upload-receipt.component.html',
  styleUrls: ['./upload-receipt.component.scss']
})
export class UploadReceiptComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  dataActiveList;

  showLoader = false
  imageBanner = "";
  orderID;
  usersForm: FormGroup;
  comment = "";
  time;
  amount;
  fromdate;
  canSave = false;
  constructor(private fb: FormBuilder, private receiptSevice: ReceiptService, private router: Router, private customerService: CustomerService) {


  }

  getPosts(orderid){
    this.orderID = orderid;
  }
  
  change() {
    if (this.amount != null && this.amount != "" && this.orderID != null && this.orderID != "" && this.imageBanner != null && this.imageBanner != "" && this.time != null && this.time != "" && this.fromdate != null && this.fromdate != "") {
      this.canSave = true;
    } else {
      this.canSave = false;
    }
  }
  ngOnInit() {

    this.usersForm = this.fb.group({
      userInput: null
    })

    if (localStorage.getItem('id') != null) {
      this.showLoader = true

      this.customerService.getListOrderActive(localStorage.getItem('id')).subscribe(
        data => {
          this.showLoader = false
          // if (data['status'] = 200) {
          //   this.showLoader = false
          //   this.modalIcon = "fas fa-check-circle iGreen"
          //   this.colorH = "iGreen"
          //   this.modalResult = "สำเร็จ!"
          //   this.modalMessage = "อัพโหลดใบเสร็จสำเร็จกรุณารอการยืนยันจากเจ้าหน้าที่";
          //   this.showModal();
          // } else {
          //   this.showLoader = false
          //   this.modalMessage = "อัพโหลดใบเสร็จไม่สำเร็จกรุณาลองอีกครั้ง";
          //   this.modalResult = "ผิดพลาด";
          //   this.modalIcon = "fas fa-times-circle iRed";
          //   this.colorH = "iRed";
          //   this.showModal();
          // }
          this.dataActiveList = data;
          console.log(data);
        },
        error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        })
    }

  }
  onRemoved(e) {
    this.imageBanner = '';
    this.change()
  }
  onUploadFinished(e) {
    this.imageBanner = e.src;
    this.change()
  }
  save() {
    if (localStorage.getItem('id') != null) {
      this.showLoader = true
      let receiptModel = new ReceiptModel;
      receiptModel.orderid = this.orderID.trim();
      receiptModel.receiptimage = this.imageBanner.trim();
      receiptModel.remark = this.comment;
      receiptModel.amount = this.amount;
      receiptModel.userid = parseInt(localStorage.getItem('id'));
      let newDate = new Date(new DateFormatPipe3('en').transform((this.fromdate)) + 'T' + this.time + 'Z');
      receiptModel.datereceipt = newDate;

      this.receiptSevice.addReceipt(receiptModel).subscribe(
        data => {
          this.showLoader = false
          if (data['status'] = 200) {
            this.showLoader = false
            this.modalIcon = "fas fa-check-circle iGreen"
            this.colorH = "iGreen"
            this.modalResult = "สำเร็จ!"
            this.modalMessage = "อัพโหลดใบเสร็จสำเร็จกรุณารอการยืนยันจากเจ้าหน้าที่";
            this.showModal();
          } else {
            this.showLoader = false
            this.modalMessage = "อัพโหลดใบเสร็จไม่สำเร็จกรุณาลองอีกครั้ง";
            this.modalResult = "ผิดพลาด";
            this.modalIcon = "fas fa-times-circle iRed";
            this.colorH = "iRed";
            this.showModal();
          }

        },
        error => {
          this.showLoader = false
          this.modalMessage = "Please check your internet.";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
          this.showModal();
        })
    } else {
      this.showLoader = false
      this.modalMessage = "โปรดเข้าสู่ระบบก่อนทำการแจ้งชำระเงิน";
      this.modalResult = "มีข้อผิดพลาด !";
      this.modalIcon = "fas fa-times-circle iRed";
      this.colorH = "iRed";
      this.showModal();
    }
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    this.router.navigate(['/index']);
  }
}