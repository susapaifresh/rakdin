import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailEditConfigComponent } from './email-edit.component';

describe('UserComponent', () => {
  let component: EmailEditConfigComponent;
  let fixture: ComponentFixture<EmailEditConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailEditConfigComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailEditConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
