import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailEditService } from '../../shared/service/emailedit';
import { EmailConfigDetail, EmailConfigEmailListRole } from '../../shared/model/emailconfig';
import { FormsModule } from '@angular/forms';

export interface UserRole {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './email-edit.component.html',
  styleUrls: ['./email-edit.component.css'],
  animations: [routerTransition()],
})

export class EmailEditConfigComponent implements OnInit {
  blank: string | '';
  form: FormsModule;

  dataSource: EmailConfigDetail = new EmailConfigDetail;
  dataSourceTmp: EmailConfigDetail = new EmailConfigDetail;
  submitted: boolean = false;
  sendto;

  onSubmit() {
    this.dataSourceTmp = this.dataSource;
    this.emailService.updateEmailConfig(this.dataSourceTmp).subscribe(
      data => {
        this.router.navigate(['/email']);
      },
      error => {

      });
  }

  constructor( public router: Router, public route: ActivatedRoute, private emailService: EmailEditService) {
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      let id: number = params['id'];
      this.emailService.getEmailConfigByID(id).subscribe(
        data => {
          this.dataSource = data;
        },
        error => {

        });
    });
  }
  cancel() {
    this.router.navigate(['/email']);
  }
}