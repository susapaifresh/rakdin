
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FirstPageService } from 'src/app/shared/service/firstpage';
@Component({
  selector: 'app-add-firstpage',
  templateUrl: './add-firstpage.component.html',
  styleUrls: ['./add-firstpage.component.scss']
})
export class AddFirstPageComponent implements OnInit {
  /////modal
  modalMessage;
  modalResult;
  modalIcon;
  colorH;

  showLoader = false
  labelText = "test";
  farmername;
  farmersector;
  form: FormGroup;
  url = '';

  imageBanner1 = "";
  imageBanner2 = "";
  imageBanner3 = "";
  imageBanner4 = "";

  aboutUs = '';
  RoleDropDowns: DropdownSector[] = [];
  constructor(private addfirstPageSevice: FirstPageService, private router: Router) {

  }

  ngOnInit() {

  }
  onRemoved(e, id) {
    if (id == 1) {
      this.imageBanner1 = '';
    }
    if (id == 2) {
      this.imageBanner2 = '';
    }
    if (id == 3) {
      this.imageBanner3 = '';
    }
    if (id == 4) {
      this.imageBanner4 = '';
    }
  }
  onUploadFinished(e, id) {
    if (id == 1) {
      this.imageBanner1 = e.src;
    }
    if (id == 2) {
      this.imageBanner2 = e.src;
    }
    if (id == 3) {
      this.imageBanner3 = e.src;
    }
    if (id == 4) {
      this.imageBanner4 = e.src;
    }
  }
  save() {
    this.showLoader = true

    this.aboutUs = document.querySelector(".ql-editor").innerHTML;
    this.addfirstPageSevice.addFirstPage(this.aboutUs, this.imageBanner1, this.imageBanner2, this.imageBanner3, this.imageBanner4).subscribe(
      data => {
        this.showLoader = false

        console.log(data);
        if(data.status == 200){
          this.showLoader = false
          this.modalIcon = "fas fa-check-circle iGreen"
          this.colorH = "iGreen"
          this.modalResult = "Success !"
          this.modalMessage = data.text;
          
        }else{
          this.showLoader = false
          this.modalMessage = "";
          this.modalResult = "Failed !";
          this.modalIcon = "fas fa-times-circle iRed";
          this.colorH = "iRed";
        }
        this.showModal();
      },
      error => {
        this.showLoader = false
        this.modalMessage = "Please check your internet.";
        this.modalResult = "Failed !";
        this.modalIcon = "fas fa-times-circle iRed";
        this.colorH = "iRed";
        this.showModal();
      });
  }
  showModal() {
    document.getElementById("btnModal").click()
  }
  closeModal() {
    document.getElementById("btnModal").click()
    location.reload();
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        let target: any = event.target; //<-- This (any) will tell compiler to shut up!
        let content: string = target.result;
        this.url = content;
      }
    }
  }
}
export class DropdownSector {
  id: number;
  name: string;
}