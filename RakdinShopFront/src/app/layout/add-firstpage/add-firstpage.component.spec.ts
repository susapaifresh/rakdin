import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFirstPageComponent } from './add-firstpage.component';

describe('LoginComponent', () => {
  let component: AddFirstPageComponent;
  let fixture: ComponentFixture<AddFirstPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddFirstPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFirstPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
