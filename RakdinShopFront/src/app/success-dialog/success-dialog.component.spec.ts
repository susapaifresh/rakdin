import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessDialogConfigComponent } from './success-dialog.component';

describe('UserComponent', () => {
  let component: SuccessDialogConfigComponent;
  let fixture: ComponentFixture<SuccessDialogConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SuccessDialogConfigComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessDialogConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
