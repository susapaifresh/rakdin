import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-user',
  templateUrl: './success-dialog.component.html',
  styleUrls: ['./success-dialog.component.css'],
})

export class SuccessDialogConfigComponent implements OnInit {
  constructor(
    private dialogRef: MatDialogRef<SuccessDialogConfigComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {

  }

  public closeDialog() {
    this.dialogRef.close();
  }

}
