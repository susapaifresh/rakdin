﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class ProductAsyncController : Controller
    {
        // GET api/ProductAsync
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ProductQuery(db);
                var result = await query.GetAllProductAsync();
                return new OkObjectResult(result);
            }
        }

        // GET api/ProductAsync/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ProductQuery(db);
                var result = await query.FindProductByIDAsync(id);
                if (result == null)
                    return new NotFoundResult();
                return new OkObjectResult(result);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Product body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                body.Db = db;
               
                var result = await body.CreateProductAsync();
                if (result >= 0)
                {
                    var query = new ProductQuery(db);
                    var resultQuery = await query.FindProductByIDAsync(result);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    // returnmessage.obj = resultQuery;
                    returnmessage.obj = JsonConvert.SerializeObject(resultQuery);
                    returnmessage.text = "Create Product success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                   // dynamic results = JsonConvert.DeserializeObject<dynamic>(json);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 404;
                    returnmessage.obj = "";
                    returnmessage.text = "Create Product error";

                    string json = JsonConvert.SerializeObject(returnmessage);
                    return new OkObjectResult(returnmessage);
                }
            }
        }

        // PUT api/async/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOne(int id, [FromBody]Product body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();

                var query = new ProductQuery(db);
                var result = await query.UpdateAsync(id, body.productname, body.price, body.price2, body.group, body.unit, body.ownerfarmer,body.picture, body.promotion);

                return new OkObjectResult(result);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOne(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ProductQuery(db);
                var result = await query.DeleteAsync(id);
                if (result == 1)
                {
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    returnmessage.obj = JsonConvert.SerializeObject(result);
                    returnmessage.text = "Delete Success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 401;
                    returnmessage.obj = JsonConvert.SerializeObject(result);
                    returnmessage.text = "Not Found";
                    return new OkObjectResult(returnmessage);
                }
            }
        }

        // DELETE api/async
        //[HttpDelete]
        //public async Task<IActionResult> DeleteAll()
        //{
        //    using (var db = new AppDb())
        //    {
        //        await db.Connection.OpenAsync();
        //        var query = new CustomerQuery(db);
        //        await query.DeleteAllAsync();
        //        return new OkResult();
        //    }
        //}
    }
}
