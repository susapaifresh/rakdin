﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class FarmerAsyncController : Controller
    {
        // GET api/FarmerAsync
        [HttpGet]
        public async Task<IActionResult> GetLatest()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new FarmerQuery(db);
                var result = await query.GetAllFarmerAsync();
                return new OkObjectResult(result);
            }
        }

        // GET api/FarmerAsync/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new FarmerQuery(db);
                var result = await query.FindFarmerByIDAsync(id);
                if (result == null)
                    return new NotFoundResult();
                return new OkObjectResult(result);
            }
        }

        // POST api/createfarmerasync
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Farmer body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                body.Db = db;
               
                var result = await body.CreateFarmerAsync();
                if (result >= 0)
                {
                    var query = new FarmerQuery(db);
                    var resultQuery = await query.FindFarmerByIDAsync(result);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    // returnmessage.obj = resultQuery;
                    returnmessage.obj = JsonConvert.SerializeObject(resultQuery);
                    returnmessage.text = "Create Farmer success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                   // dynamic results = JsonConvert.DeserializeObject<dynamic>(json);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 404;
                    returnmessage.obj = "";
                    returnmessage.text ="Create Farmer error";

                    string json = JsonConvert.SerializeObject(returnmessage);
                    return new OkObjectResult(returnmessage);
                }
            }
        }

        // PUT api/async/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutOne(int id, [FromBody]Customer body)
        //{
        //    using (var db = new AppDb())
        //    {
        //        await db.Connection.OpenAsync();
        //        var query = new CustomerQuery(db);
        //        var result = await query.FindCustomerByIDAsync(id);
        //        if (result == null)
        //            return new NotFoundResult();
        //        result.nameuser = body.nameuser;
        //        result.district = body.district;
        //        await result.UpdateAsync();
        //        return new OkObjectResult(result);
        //    }
        //}

        // DELETE api/async/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOne(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                var result = await query.FindCustomerByIDAsync(id);
                if (result == null)
                    return new NotFoundResult();
                await result.DeleteAsync();
                return new OkResult();
            }
        }

        // DELETE api/async
        [HttpDelete]
        public async Task<IActionResult> DeleteAll()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                await query.DeleteAllAsync();
                return new OkResult();
            }
        }
    }
}
