﻿      using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class CustomerAsyncController : Controller
    {
        // GET api/async
        [HttpGet]
        public async Task<IActionResult> GetLatest()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                var result = await query.LatestPostsAsync();
                return new OkObjectResult(result);
            }
        }

        // GET api/async/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserDetail(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                var result = await query.FindCustomerByIDAsync(id);
                if (result == null)
                    return new NotFoundResult();
                return new OkObjectResult(result);
            }
        }

        // POST api/customerasync
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Customer body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                body.Db = db;
               
                var result = await body.CreateCustomerAsync();
                if (result >= 0)
                {
                    var query = new CustomerQuery(db);
                    var resultQuery = await query.FindCustomerByIDAsync(result);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    // returnmessage.obj = resultQuery;
                    returnmessage.obj = JsonConvert.SerializeObject(resultQuery);
                    returnmessage.text = "Create User success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                   // dynamic results = JsonConvert.DeserializeObject<dynamic>(json);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 404;
                    returnmessage.obj = "";
                    returnmessage.text ="Create User error";

                    string json = JsonConvert.SerializeObject(returnmessage);
                    return new OkObjectResult(returnmessage);
                }
            }
        }

        // PUT api/async/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOne(int id, [FromBody]Customer body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                var result = await query.FindCustomerByIDAsync(id);
                if (result == null)
                    return new NotFoundResult();
                
                return new OkObjectResult(await result.UpdateAsync(body.nameuser, body.phone, body.tel, id.ToString()));
            }
        }

        // DELETE api/async/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOne(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                var result = await query.FindCustomerByIDAsync(id);
                if (result == null)
                    return new NotFoundResult();
                await result.DeleteAsync();
                return new OkResult();
            }
        }

        // DELETE api/async
        [HttpDelete]
        public async Task<IActionResult> DeleteAll()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new CustomerQuery(db);
                await query.DeleteAllAsync();
                return new OkResult();
            }
        }
    }
}
