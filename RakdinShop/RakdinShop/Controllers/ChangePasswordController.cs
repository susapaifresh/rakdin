﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class ChangePasswordController : Controller
    {
        
        [HttpGet("{urlcheck}")]
        public async Task<IActionResult> GetUrl(string urlcheck)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ChangePasswordQuery(db);
                var result = await query.CheckUrl(urlcheck);
                if (result == null)
                {
                     return new NotFoundResult();
                }
                else
                {
                    return new OkObjectResult(result);
                }
            }
        }

        public class ChangePassModel
        {
            public int id { get; set; }
            public string password { get; set; }
        }

        [HttpPost]
       // [Route("changepass")]
        public async Task<IActionResult> Post([FromBody]ChangePassModel changePassModel)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ChangePasswordQuery(db);

                var result = await query.UpdateAsync(changePassModel.id, changePassModel.password);
                if (result >= 0)
                {
                   
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    returnmessage.obj = JsonConvert.SerializeObject(null);
                    returnmessage.text = "Update Password success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                   // dynamic results = JsonConvert.DeserializeObject<dynamic>(json);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 404;
                    returnmessage.obj = "";
                    returnmessage.text ="Update Password error";

                    string json = JsonConvert.SerializeObject(returnmessage);
                    return new OkObjectResult(returnmessage);
                }
            }
        }

        
    }
}
