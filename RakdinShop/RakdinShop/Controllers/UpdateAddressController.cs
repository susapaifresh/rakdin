﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class UpdateAddressController : Controller
    {
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAddress(int id, [FromBody]UpdateAddress body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new Address(db);

                 var result =   await query.UpdateAsync(id.ToString(), body.homenumber, body.subdistrict, body.district, body.province, body.postcode, body.addNo);

                return new OkObjectResult(result);
            }
        }
        
    }

    public class UpdateAddress
    {
        public string homenumber { get; set; }
        public string subdistrict { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string postcode { get; set; }
        public string addNo { get; set; }

    }
}
