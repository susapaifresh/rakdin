﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class OrderReportGetListController : Controller
    {
       
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(String id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new OrderQuery(db);
                var result = await query.GetOrderListActive(id);
                if (result == null)
                    return new NotFoundResult();
                return new OkObjectResult(result);
            }
        }
    }
}
