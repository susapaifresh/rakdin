﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class OrderReceiptReportController : Controller
    {
        [HttpPost]
        public async Task<IActionResult> GetReport([FromBody]DateQueryReceiptModel body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ReceiptQuery(db);
                var result = await query.FindReceipByDate(body.startdate, body.enddate, body.orderID);
                return new OkObjectResult(result);
            }
        }
    }
}