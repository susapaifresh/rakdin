﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class CustomerLoginController : Controller
    {
       
        // POST api/customerlogin
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerLogin body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                body.Db = db;
               
                var result = await body.FindCustomerByLogin(body.email,body.password);
                if (result != null)
                {
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    returnmessage.obj = JsonConvert.SerializeObject(result);
                    returnmessage.text = "Login Success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 401;
                    returnmessage.obj = JsonConvert.SerializeObject(result);
                    returnmessage.text = "Not Autorize";
                    return new OkObjectResult(returnmessage);
                }
            }
        }
    }
}
