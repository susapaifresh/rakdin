﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class OrderReceiptController : Controller
    {
        // POST api/order
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]OrderReceiptModel body)
        {
            using (var db = new AppDb())
            {
                var result = 0;
                await db.Connection.OpenAsync();

                    body.Db = db;
                    result = await body.CreateOrderReceiptAsync();

                if (result >= 0)
                {
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    // returnmessage.obj = resultQuery;
                    returnmessage.obj = JsonConvert.SerializeObject(null);
                    returnmessage.text = "Create order receipt success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                    // dynamic results = JsonConvert.DeserializeObject<dynamic>(json);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 404;
                    returnmessage.obj = "";
                    returnmessage.text = result.ToString();

                    string json = JsonConvert.SerializeObject(returnmessage);
                    return new OkObjectResult(returnmessage);
                }
            }
        }
    }
}