﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class AddFirstPageController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> GetLatest()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new AddFirstPageQuery(db);
                var result = await query.FindFirstPageAsync();
                return new OkObjectResult(result);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddFirstPage body)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                body.Db = db;
               
                var result = await body.CreateFirstPage();
                if (result >= 0)
                {
                    var query = new AddFirstPageQuery(db);
                    var resultQuery = await query.FindFirstPageAsync();
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = 200;
                    // returnmessage.obj = resultQuery;
                    returnmessage.obj = JsonConvert.SerializeObject(resultQuery);
                    returnmessage.text = "Create First Page success";
                    return new OkObjectResult(returnmessage);
                }
                else
                {
                   // dynamic results = JsonConvert.DeserializeObject<dynamic>(json);
                    ReturnMessage returnmessage = new ReturnMessage();
                    returnmessage.status = result;
                    returnmessage.obj = "";
                    returnmessage.text = "Create First Page error";

                    string json = JsonConvert.SerializeObject(returnmessage);
                    return new OkObjectResult(returnmessage);
                }
            }
        }

        // PUT api/async/5
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutOne(int id, [FromBody]Customer body)
        //{
        //    using (var db = new AppDb())
        //    {
        //        await db.Connection.OpenAsync();
        //        var query = new CustomerQuery(db);
        //        var result = await query.FindCustomerByIDAsync(id);
        //        if (result == null)
        //            return new NotFoundResult();
        //        result.nameuser = body.nameuser;
        //        result.district = body.district;
        //        await result.UpdateAsync();
        //        return new OkObjectResult(result);
        //    }
        //}
        
    }
}
