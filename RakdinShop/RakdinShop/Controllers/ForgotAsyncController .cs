﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;
using System.Net.Mail;
using System.Text;
using System;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class ForgotAsyncController : Controller
    {
        
        [HttpGet("{email}")]
        public async Task<IActionResult> GetEmail(string email)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ForgotQuery(db);
                var result = await query.FindCustomerByEmail(email);
                if (result == null)
                {
                    return new NotFoundResult();
                }
                else
                {

                    try
                    {
                        // Command line argument must the the SMTP host.
                        SmtpClient client = new SmtpClient();
                        client.Port = 25;
                        client.UseDefaultCredentials = false;
                        client.Host = "mail.tsdevelopment.co.th";
                        client.Timeout = 10000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.Credentials = new System.Net.NetworkCredential("tsdbackup@tsdevelopment.co.th", "D3v0t3D%$");

                        MailMessage mm = new MailMessage("tsdbackup@tsdevelopment.co.th", result.email, "ลิ้งค์สำหรับเปลี่ยนรหัสผ่าน", "<h2>คลิกข้อความด้านล่างเพื่อทำการเปลี่ยนรหัสผ่าน</h2><a href='http://202.57.191.75/#/editpassword/" + result.phone+"'>คลิก</a>");
                        mm.BodyEncoding = UTF8Encoding.UTF8;
                        mm.IsBodyHtml = true;

                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                    }
                    catch(Exception e)
                    {

                    }
                    

                    return new OkObjectResult(result);
                }
            }
        }
    }
}
