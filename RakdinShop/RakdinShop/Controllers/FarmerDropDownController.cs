﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class FarmerDropDownController : Controller
    {
        // GET api/FarmerAsync
        [HttpGet]
        public async Task<IActionResult> GetLatest()
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new DropDownFarmer(db);
                var result = await query.GetAllDropDown();
                return new OkObjectResult(result);
            }
        }

      
    }
}
