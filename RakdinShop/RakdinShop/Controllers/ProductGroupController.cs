﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RakdinShop.Models;
using RakdinShop.Performance;

namespace RakdinShop.Controllers
{
    [Route("api/[controller]")]
    public class ProductGroupController : Controller
    {

        // GET api/ProductAsync/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            using (var db = new AppDb())
            {
                await db.Connection.OpenAsync();
                var query = new ProductQuery(db);
                var result = await query.FindProductByGroupAsync(id);
                if (result == null)
                    return new NotFoundResult();
                return new OkObjectResult(result);
            }
        }
    }
}
