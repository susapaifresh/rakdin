﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Text;

namespace RakdinShop.Models
{
    public class CustomerDetail
    {
        public int id { get; set; }
        public int role { get; set; }
        public string email { get; set; }
        public string nameuser { get; set; }
        public string phone { get; set; }
        public string homenumber { get; set; }
        public string subdistrict { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string postcode { get; set; }
        public string homenumber2 { get; set; }
        public string subdistrict2 { get; set; }
        public string district2 { get; set; }
        public string province2 { get; set; }
        public string postcode2 { get; set; }
        public string homenumber3 { get; set; }
        public string subdistrict3 { get; set; }
        public string district3 { get; set; }
        public string province3 { get; set; }
        public string postcode3 { get; set; }
        public string tel { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }
        [JsonIgnore]
        public AppDb Db { get; set; }

        public CustomerDetail(AppDb db = null)
        {
            Db = db;
        }
        //encode base 64
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        //encode MD5
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public async Task<int> CreateCustomerAsync()
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `customer`(`email`, `password`, `nameuser`, `phone`, `homenumber`,
                            `subdistrict`, `district`, `province`, `postcode`)
                        VALUES (@email, @password, @nameuser, @phone, @homenumber,
                            @subdistrict, @district, @province, @postcode)";
                BindParams(cmd);
                int id = await cmd.ExecuteNonQueryAsync();
                return id = (int)cmd.LastInsertedId;

            }
            catch
            {
                return -1;
                throw;
            }
            // return id = (int)cmd.LastInsertedId;
        }

        public async Task<int> UpdateAsync(string nameuser, string phone, string tel, string id)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;

                cmd.CommandText = @"UPDATE `customer` SET `nameuser` = @nameuser, `phone` = @phone , `tel` = @tel WHERE `Id` = @id;";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@nameuser",
                    DbType = DbType.String,
                    Value = nameuser,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@phone",
                    DbType = DbType.String,
                    Value = phone,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@tel",
                    DbType = DbType.String,
                    Value = tel,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.String,
                    Value = id,
                });
                await cmd.ExecuteNonQueryAsync();
                return 200;
            }catch(Exception e)
            {
                return 404;
            }

        }

        public async Task DeleteAsync()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"DELETE FROM `BlogPost` WHERE `Id` = @id;";
            BindId(cmd);
            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = DbType.String,
                Value = email,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@nameuser",
                DbType = DbType.String,
                Value = nameuser,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@phone",
                DbType = DbType.String,
                Value = phone,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@homenumber",
                DbType = DbType.String,
                Value = homenumber,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@subdistrict",
                DbType = DbType.String,
                Value = subdistrict,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@district",
                DbType = DbType.String,
                Value = district,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@province",
                DbType = DbType.String,
                Value = province,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@postcode",
                DbType = DbType.String,
                Value = postcode,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@createdate",
                DbType = DbType.String,
                Value = createdate,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@updatedate",
                DbType = DbType.String,
                Value = updatedate,
            });
                   }
    }
}
