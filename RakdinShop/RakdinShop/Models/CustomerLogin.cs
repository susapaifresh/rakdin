﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Data.Common;
using System.Collections.Generic;

namespace RakdinShop.Models
{
    public class CustomerLogin
    {
        public string email { get; set; }
        public string password { get; set; }
       
        [JsonIgnore]
        public AppDb Db { get; set; }

        public CustomerLogin(AppDb db = null)
        {
            Db = db;
        }

        public async Task<Customer> FindCustomerByLogin(string email, string password)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM `customer` WHERE `email` = @email and `password` = @password";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = DbType.String,
                Value = email,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@password",
                DbType = DbType.String,
                Value = password,
            });
            var result = await ReadCustomerDetialAsync(await cmd.ExecuteReaderAsync());
            return result[0];
        }

        private async Task<List<Customer>> ReadCustomerDetialAsync(DbDataReader reader)
        {
            var posts = new List<Customer>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new Customer(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        email = await reader.GetFieldValueAsync<string>(1),
                        password = await reader.GetFieldValueAsync<string>(2),
                        nameuser = await reader.GetFieldValueAsync<string>(3),
                        phone = await reader.GetFieldValueAsync<string>(4),
                        homenumber = await reader.GetFieldValueAsync<string>(5),
                        subdistrict = await reader.GetFieldValueAsync<string>(6),
                        district = await reader.GetFieldValueAsync<string>(7),
                        province = await reader.GetFieldValueAsync<string>(8),
                        postcode = await reader.GetFieldValueAsync<string>(9),
                        createdate = await reader.GetFieldValueAsync<DateTime>(10),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(11),
                        role = await reader.GetFieldValueAsync<int>(12),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}
