﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class ProductQuery
    {

        public readonly AppDb Db;
        public ProductQuery(AppDb db)
        {
            Db = db;
        }
        public async Task<int> UpdateAsync(int id, string productname, float price, float price2, int group, int unit, int ownerfarmer, string picture, string promotion)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `product` SET `productname` = @productname ,`price` = @price,`price2` =@price2,`group` =@group
,`unit` =@unit,`ownerfarmer` =@ownerfarmer,`picture` = @picture,`promotion` = @promotion WHERE `id` = @id";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@productname",
                    DbType = DbType.String,
                    Value = productname,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@promotion",
                    DbType = DbType.String,
                    Value = promotion,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@price",
                    DbType = DbType.Single,
                    Value = price,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@price2",
                    DbType = DbType.Single,
                    Value = price2,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@group",
                    DbType = DbType.Int32,
                    Value = group,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@unit",
                    DbType = DbType.Int32,
                    Value = unit,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@ownerfarmer",
                    DbType = DbType.Int32,
                    Value = ownerfarmer,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@picture",
                    DbType = DbType.String,
                    Value = picture,
                });
                await cmd.ExecuteNonQueryAsync();
                return 200;
            }
            catch (Exception ex)
            {
                return 404;

            }
        }

        public async Task<ProductResult> FindProductByIDAsync(int id)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;

            cmd.CommandText = @"
SELECT `product`.id, `product`.productname,`product`.price,`product`.price2,`group`.name as `group`
,`product`.picture,`unit`.name as `unit`,`farmer`.name as `ownerfarmer`,`product`.ownerfarmer as `ownerfarmerid`,`product`.unit as `unitid`,`product`.group as `groupid`, `product`.promotion,`group`.`precis` as `groupprecis`
from (((`product`
INNER JOIN `group` ON `product`.group = `group`.id)
INNER JOIN `unit` ON `product`.unit = `unit`.id)
INNER JOIN `farmer` ON `product`.ownerfarmer = `farmer`.id)
  WHERE `product`.id = @id";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadProductDetialAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }

        public async Task<List<ProductResult>> FindProductByGroupAsync(int id)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;

            cmd.CommandText = @"
SELECT `product`.id, `product`.productname,`product`.price,`product`.price2,`group`.name as `group`
,`product`.picture,`unit`.name as `unit`,`farmer`.name as `ownerfarmer`,`product`.ownerfarmer as `ownerfarmerid`,`product`.unit as `unitid`,`product`.group as `groupid`, `product`.promotion,`group`.`precis` as `groupprecis`
from (((`product`
INNER JOIN `group` ON `product`.group = `group`.id)
INNER JOIN `unit` ON `product`.unit = `unit`.id)
INNER JOIN `farmer` ON `product`.ownerfarmer = `farmer`.id)
  WHERE `product`.group = @id";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadProductDetialAsync(await cmd.ExecuteReaderAsync());
            return result;
        }

        public async Task<List<ProductResult>> GetAllProductAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"
SELECT `product`.id, `product`.productname,`product`.price,`product`.price2,`group`.name as `group`
,`product`.picture,`unit`.name as `unit`,`farmer`.name as `ownerfarmer`,`product`.ownerfarmer as `ownerfarmerid`,`product`.unit as `unitid`,`product`.group as `groupid`, `product`.promotion,`group`.`precis` as `groupprecis`
from (((`product`
INNER JOIN `group` ON `product`.group = `group`.id)
INNER JOIN `unit` ON `product`.unit = `unit`.id)
INNER JOIN `farmer` ON `product`.ownerfarmer = `farmer`.id)
 ORDER BY `product`.id";
            return await ReadProductDetialAsync(await cmd.ExecuteReaderAsync());
        }

        public async Task<int> DeleteAsync(int id)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM `product` WHERE `product`.id = @id;";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                var result = await cmd.ExecuteNonQueryAsync();
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        private async Task<List<ProductResult>> ReadProductDetialAsync(DbDataReader reader)
        {
            var posts = new List<ProductResult>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new ProductResult(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        productname = await reader.GetFieldValueAsync<string>(1),
                        price = await reader.GetFieldValueAsync<float>(2),
                        price2 = await reader.GetFieldValueAsync<float>(3),
                        group = await reader.GetFieldValueAsync<string>(4),
                        picture = await reader.GetFieldValueAsync<string>(5),
                        unit = await reader.GetFieldValueAsync<string>(6),
                        ownerfarmer = await reader.GetFieldValueAsync<string>(7),
                        ownerfarmerid = await reader.GetFieldValueAsync<int>(8),
                        unitid = await reader.GetFieldValueAsync<int>(9),
                        groupid = await reader.GetFieldValueAsync<int>(10),
                        promotion = await reader.GetFieldValueAsync<string>(11),
                        groupprecis = await reader.GetFieldValueAsync<string>(12),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}