﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class CustomerQuery
    {

        public readonly AppDb Db;
        public CustomerQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<CustomerDetail> FindCustomerByIDAsync(int id)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM `customer` WHERE `id` = @id";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadCustomerDetial(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }


        public async Task<List<Customer>> LatestPostsAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `customer` ORDER BY `id` DESC LIMIT 10;";
            return await ReadCustomerDetialAsync(await cmd.ExecuteReaderAsync());
        }

        public async Task DeleteAllAsync()
        {
            var txn = await Db.Connection.BeginTransactionAsync();
            try
            {
                var cmd = Db.Connection.CreateCommand();
                cmd.CommandText = @"DELETE FROM `BlogPost`";
                await cmd.ExecuteNonQueryAsync();
                await txn.CommitAsync();
            }
            catch
            {
                await txn.RollbackAsync();
                throw;
            }
        }

        private async Task<List<Customer>> ReadCustomerDetialAsync(DbDataReader reader)
        {
            var posts = new List<Customer>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new Customer(Db)
                    {
                        id = await reader.GetFieldValueAsync<Int32>(0),
                        email = await reader.GetFieldValueAsync<string>(1),
                        password = await reader.GetFieldValueAsync<string>(2),
                        nameuser = await reader.GetFieldValueAsync<string>(3),
                        phone = await reader.GetFieldValueAsync<string>(4),
                        homenumber = await reader.GetFieldValueAsync<string>(5),
                        subdistrict = await reader.GetFieldValueAsync<string>(6),
                        district = await reader.GetFieldValueAsync<string>(7),
                        province = await reader.GetFieldValueAsync<string>(8),
                        postcode = await reader.GetFieldValueAsync<string>(9),
                        createdate = await reader.GetFieldValueAsync<DateTime>(10),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(11),
                        role = await reader.GetFieldValueAsync<int>(12),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
        
        private async Task<List<CustomerDetail>> ReadCustomerDetial(DbDataReader reader)
        {
            var posts = new List<CustomerDetail>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new CustomerDetail(Db)
                    {
                        id = await reader.GetFieldValueAsync<Int32>(0),
                        email = await reader.GetFieldValueAsync<string>(1),
                        nameuser = await reader.GetFieldValueAsync<string>(3),
                        phone = await reader.GetFieldValueAsync<string>(4),
                        homenumber = await reader.GetFieldValueAsync<string>(5),
                        subdistrict = await reader.GetFieldValueAsync<string>(6),
                        district = await reader.GetFieldValueAsync<string>(7),
                        province = await reader.GetFieldValueAsync<string>(8),
                        postcode = await reader.GetFieldValueAsync<string>(9),
                        createdate = await reader.GetFieldValueAsync<DateTime>(10),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(11),
                        role = await reader.GetFieldValueAsync<int>(12),
                        tel = await reader.GetFieldValueAsync<string>(13),
                        homenumber2 = await reader.GetFieldValueAsync<string>(14),
                        subdistrict2 = await reader.GetFieldValueAsync<string>(15),
                        district2 = await reader.GetFieldValueAsync<string>(16),
                        province2 = await reader.GetFieldValueAsync<string>(17),
                        postcode2 = await reader.GetFieldValueAsync<string>(18),
                        homenumber3 = await reader.GetFieldValueAsync<string>(19),
                        subdistrict3 = await reader.GetFieldValueAsync<string>(20),
                        district3 = await reader.GetFieldValueAsync<string>(21),
                        province3 = await reader.GetFieldValueAsync<string>(22),
                        postcode3 = await reader.GetFieldValueAsync<string>(23),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}