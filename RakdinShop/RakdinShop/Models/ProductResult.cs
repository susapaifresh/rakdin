﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Text;

namespace RakdinShop.Models
{
    public class ProductResult
    {
        public int id { get; set; }
        public int ownerfarmerid { get; set; }
        public int groupid { get; set; }
        public int unitid { get; set; }
        public string productname { get; set; }
        public string promotion { get; set; }
        public float price { get; set; }
        public float price2 { get; set; }
        public string group { get; set; }
        public string groupprecis { get; set; }
        public string picture { get; set; }
        public string unit { get; set; }
        public string ownerfarmer { get; set; }
        public Boolean isactive { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }
        [JsonIgnore]
        public AppDb Db { get; set; }

        public ProductResult(AppDb db = null)
        {
            Db = db;
        }
       
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@productname",
                DbType = DbType.String,
                Value = productname,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@groupprecis",
                DbType = DbType.String,
                Value = groupprecis,
            }); cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@promotion",
                DbType = DbType.String,
                Value = promotion,
            });
            cmd.Parameters.Add(new MySqlParameter
            {  

            ParameterName = "@price",
                DbType = DbType.Single,
                Value = price,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@price2",
                DbType = DbType.Single,
                Value = price2,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@group",
                DbType = DbType.String,
                Value = group,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@picture",
                DbType = DbType.String,
                Value = picture,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@unit",
                DbType = DbType.String,
                Value = unit,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ownerfarmer",
                DbType = DbType.String,
                Value = unit,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ownerfarmerid",
                DbType = DbType.Int32,
                Value = ownerfarmerid,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@groupid",
                DbType = DbType.Int32,
                Value = groupid,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@unitid",
                DbType = DbType.Int32,
                Value = unitid,
            });
        }
    }
}
