﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RakdinShop.Models
{
    public class ReturnMessage
    {
        public int status { get; set; }
        public string text { get; set; }
        public string obj { get; set; }

        public ReturnMessage()
        {
        }
            public ReturnMessage(int status,string text)
        {
            this.status = status;
            this.text = text;
        }
    }
}
