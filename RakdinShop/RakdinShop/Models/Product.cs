﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Text;

namespace RakdinShop.Models
{
    public class Product
    {
        public int id { get; set; }
        public string productname { get; set; }
        public string promotion { get; set; }
        public float price { get; set; }
        public float price2 { get; set; }
        public int group { get; set; }
        public string picture { get; set; }
        public int unit { get; set; }
        public int ownerfarmer { get; set; }
        public Boolean isactive { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }
        [JsonIgnore]
        public AppDb Db { get; set; }

        public Product(AppDb db = null)
        {
            Db = db;
        }
       
        public async Task<int> CreateProductAsync()
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `product`(`productname`, `price`, `price2`, `group`, `picture`, `unit`, `ownerfarmer`, `promotion`)
                        VALUES (@productname, @price, @price2, @group, @picture, @unit, @ownerfarmer, @promotion)";
                BindParams(cmd);
                int id = await cmd.ExecuteNonQueryAsync();
                return id = (int)cmd.LastInsertedId;

            }
            catch
            {
                return -1;
                throw;
            }
            // return id = (int)cmd.LastInsertedId;
        }

        //public async Task UpdateAsync()
        //{
        //    var cmd = Db.Connection.CreateCommand() as MySqlCommand;
        //    cmd.CommandText = @"UPDATE `BlogPost` SET `Title` = @title, `Content` = @content WHERE `Id` = @id;";
        //    BindParams(cmd);
        //    BindId(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //}

        //public async Task DeleteAsync()
        //{
        //    var cmd = Db.Connection.CreateCommand() as MySqlCommand;
        //    cmd.CommandText = @"DELETE FROM `BlogPost` WHERE `Id` = @id;";
        //    BindId(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //}

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@productname",
                DbType = DbType.String,
                Value = productname,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@promotion",
                DbType = DbType.String,
                Value = promotion,
            });
            cmd.Parameters.Add(new MySqlParameter
            {  

            ParameterName = "@price",
                DbType = DbType.Single,
                Value = price,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@price2",
                DbType = DbType.Single,
                Value = price2,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@group",
                DbType = DbType.Int32,
                Value = group,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@picture",
                DbType = DbType.String,
                Value = picture,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@unit",
                DbType = DbType.Int32,
                Value = unit,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@ownerfarmer",
                DbType = DbType.Int32,
                Value = ownerfarmer,
            });
        }
    }
}
