﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Data.Common;
using System.Collections.Generic;

namespace RakdinShop.Models
{
    public class OrderReceiptModel
    {
        public int id { get; set; }
        public string orderid { get; set; }
        public string receiptimage { get; set; }
        public string remark { get; set; }
        public string amount { get; set; }
        public int userid { get; set; }
        public DateTime createdate { get; set; }
        public DateTime datereceipt { get; set; }
        public DateTime updatedate { get; set; }

        [JsonIgnore]
        public AppDb Db { get; set; }

        public OrderReceiptModel(AppDb db = null)
        {
            Db = db;
        }
       
            public async Task<int> CreateOrderReceiptAsync()
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;

                cmd.CommandText = @"INSERT INTO `orderreceipt`(`orderid`,`receiptimage`, `remark`, `userid`, `datereceipt`, `amount`)
                        VALUES (@orderid, @receiptimage, @remark, @userid, @datereceipt, @amount)";
                BindParams(cmd);
                int id = await cmd.ExecuteNonQueryAsync();
                return id = (int)cmd.LastInsertedId;
            }
            catch (Exception e)
            {
                //return e.ToString();
                return -1;
                throw;
            }
            // return id = (int)cmd.LastInsertedId;
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@orderid",
                DbType = DbType.String,
                Value = orderid,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@userid",
                DbType = DbType.Int32,
                Value = userid,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@receiptimage",
                DbType = DbType.String,
                Value = receiptimage,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@remark",
                DbType = DbType.String,
                Value = remark,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@datereceipt",
                DbType = DbType.DateTime,
                Value = datereceipt,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@amount",
                DbType = DbType.String,
                Value = amount,
            });
        }
        private async Task<List<OrderModel>> ReadOrderReceiptDetialAsync(DbDataReader reader)
        {
            var posts = new List<OrderModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new OrderModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<string>(0),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}
