﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class DropDownUnit
    {

        public readonly AppDb Db;
        public DropDownUnit(AppDb db)
        {
            Db = db;
        }


        public async Task<List<UnitDropDownModel>> GetAllDropDown()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT  `id`  ,`name`  FROM `unit` ORDER BY `id`;";
            return await ReadUnitDropDownAsync(await cmd.ExecuteReaderAsync());
        }

        private async Task<List<UnitDropDownModel>> ReadUnitDropDownAsync(DbDataReader reader)
        {
            var posts = new List<UnitDropDownModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new UnitDropDownModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        name = await reader.GetFieldValueAsync<string>(1),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}