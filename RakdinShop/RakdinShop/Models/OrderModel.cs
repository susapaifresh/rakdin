﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Data.Common;
using System.Collections.Generic;

namespace RakdinShop.Models
{
    public class OrderModel
    {
        public string id { get; set; }
        public string productname { get; set; }
        public int idproduct { get; set; }
        public float price { get; set; }
        public string group { get; set; }
        public string unit { get; set; }
        public int quantity { get; set; }
        public int userid { get; set; }
        public string address { get; set; }
        public int status { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }

        [JsonIgnore]
        public AppDb Db { get; set; }

        public OrderModel(AppDb db = null)
        {
            Db = db;
        }
       
            public async Task<int> CreateOrderAsync()
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;

                cmd.CommandText = @"INSERT INTO `order`(`id`, `idproduct`, `productname`, `group`, `price`, `unit`, `quantity`,`userid`,`address`)
                        VALUES (@id, @idproduct, @productname, @group, @price, @unit,
                            @quantity, @userid,@address)";
                BindParams(cmd);
                int id = await cmd.ExecuteNonQueryAsync();
                return id = (int)cmd.LastInsertedId;
            }
            catch (Exception e)
            {
                //return e.ToString();
                return -1;
                throw;
            }
            // return id = (int)cmd.LastInsertedId;
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.String,
                Value = id,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@userid",
                DbType = DbType.Int32,
                Value = userid,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@productname",
                DbType = DbType.String,
                Value = productname,
            });

            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@group",
                DbType = DbType.String,
                Value = group,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@price",
                DbType = DbType.Single,
                Value = price,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@unit",
                DbType = DbType.String,
                Value = unit,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@quantity",
                DbType = DbType.Int32,
                Value = quantity,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@idproduct",
                DbType = DbType.Int32,
                Value = idproduct,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@address",
                DbType = DbType.String,
                Value = address,
            });

        }
        private async Task<List<OrderModel>> ReadOrderDetialAsync(DbDataReader reader)
        {
            var posts = new List<OrderModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new OrderModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<string>(0),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}
