﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Text;

namespace RakdinShop.Models
{
    public class Farmer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string sector { get; set; }
        public string detail { get; set; }
        public string imageprofile { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }
        [JsonIgnore]
        public AppDb Db { get; set; }

        public Farmer(AppDb db = null)
        {
            Db = db;
        }
       
        public async Task<int> CreateFarmerAsync()
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"INSERT INTO `farmer`(`name`, `sector`, `detail`, `imageprofile`)
                        VALUES (@name, @sector, @detail, @imageprofile)";
                BindParams(cmd);
                int id = await cmd.ExecuteNonQueryAsync();
                return id = (int)cmd.LastInsertedId;

            }
            catch
            {
                return -1;
                throw;
            }
            // return id = (int)cmd.LastInsertedId;
        }

        //public async Task UpdateAsync()
        //{
        //    var cmd = Db.Connection.CreateCommand() as MySqlCommand;
        //    cmd.CommandText = @"UPDATE `BlogPost` SET `Title` = @title, `Content` = @content WHERE `Id` = @id;";
        //    BindParams(cmd);
        //    BindId(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //}

        //public async Task DeleteAsync()
        //{
        //    var cmd = Db.Connection.CreateCommand() as MySqlCommand;
        //    cmd.CommandText = @"DELETE FROM `BlogPost` WHERE `Id` = @id;";
        //    BindId(cmd);
        //    await cmd.ExecuteNonQueryAsync();
        //}

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@name",
                DbType = DbType.String,
                Value = name,
            });
            cmd.Parameters.Add(new MySqlParameter
            {  

            ParameterName = "@sector",
                DbType = DbType.String,
                Value = sector,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@detail",
                DbType = DbType.String,
                Value = detail,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@imageprofile",
                DbType = DbType.String,
                Value = imageprofile,
            });
           
                   }
    }
}
