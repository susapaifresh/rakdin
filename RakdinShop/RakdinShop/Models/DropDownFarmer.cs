﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class DropDownFarmer
    {

        public readonly AppDb Db;
        public DropDownFarmer(AppDb db)
        {
            Db = db;
        }


        public async Task<List<FarmerDropDownModel>> GetAllDropDown()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT  `id`  ,`name`  FROM `farmer` ORDER BY `id`;";
            return await ReadFarmerDropDownAsync(await cmd.ExecuteReaderAsync());
        }

        private async Task<List<FarmerDropDownModel>> ReadFarmerDropDownAsync(DbDataReader reader)
        {
            var posts = new List<FarmerDropDownModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new FarmerDropDownModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        name = await reader.GetFieldValueAsync<string>(1),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}