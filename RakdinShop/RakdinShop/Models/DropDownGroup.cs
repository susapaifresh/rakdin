﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class DropDownGroup
    {

        public readonly AppDb Db;
        public DropDownGroup(AppDb db)
        {
            Db = db;
        }


        public async Task<List<GroupDropDownModel>> GetAllDropDown()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT  `id`  ,`name`  FROM `group` ORDER BY `id`;";
            return await ReadGroupDropDownAsync(await cmd.ExecuteReaderAsync());
        }

        private async Task<List<GroupDropDownModel>> ReadGroupDropDownAsync(DbDataReader reader)
        {
            var posts = new List<GroupDropDownModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new GroupDropDownModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        name = await reader.GetFieldValueAsync<string>(1),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}