﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class AddFirstPageQuery
    {

        public readonly AppDb Db;
        public AddFirstPageQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<AddFirstPage> FindFirstPageAsync()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM `firstpage` ORDER BY  `id` DESC LIMIT 1";
            var result = await ReadFirstPageDetialAsync(await cmd.ExecuteReaderAsync());
            return result;
        }

        private async Task<AddFirstPage> ReadFirstPageDetialAsync(DbDataReader reader)
        {
            var posts = new AddFirstPage();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new AddFirstPage(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        aboutUs = await reader.GetFieldValueAsync<string>(1),
                        imageBanner1 = await reader.GetFieldValueAsync<string>(2),
                        imageBanner2 = await reader.GetFieldValueAsync<string>(3),
                        imageBanner3 = await reader.GetFieldValueAsync<string>(4),
                        imageBanner4 = await reader.GetFieldValueAsync<string>(5),
                    };
                    posts = post;
                }
            }
            return posts;
        }
    }
}