﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;
using System.Text;

namespace RakdinShop.Models
{
    public class ChangePasswordQuery
    {
        public int id { get; set; }
        public int userid { get; set; }
        public string urlcheck { get; set; }
        public int status { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }

        public readonly AppDb Db;
        public ChangePasswordQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<CustomerDetail> CheckUrl(string url)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM `forgot` WHERE `urlcheck` = @url and `status` = 0";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@url",
                DbType = DbType.String,
                Value = url,
            });
            var result = await ReadChangePasswordDetial(await cmd.ExecuteReaderAsync());

            if (result.Count >0)
            {
                cmd.CommandText = @"SELECT * FROM `customer` WHERE `id` = '" + result[0].userid +"'";
               
                var resultuser = await ReadCustomerDetial(await cmd.ExecuteReaderAsync());
                return resultuser[0];

            }
            else
            {
                return null;
            }
        }
        public async Task<int> UpdateAsync(int id, string password)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `customer` SET `password` = @password WHERE `id` = @id";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@password",
                    DbType = DbType.String,
                    Value = password,
                });
              
                await cmd.ExecuteNonQueryAsync();

                cmd.CommandText = @"UPDATE `forgot` SET `status` = 1 WHERE `userid` = @id";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.Int32,
                    Value = id,
                });
                await cmd.ExecuteNonQueryAsync();
                return 200;
            }
            catch (Exception ex)
            {
                return 404;

            }
        }
        private async Task<List<ChangePasswordQuery>> ReadChangePasswordDetial(DbDataReader reader)
        {
            var posts = new List<ChangePasswordQuery>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new ChangePasswordQuery(Db)
                    {
                        id = await reader.GetFieldValueAsync<Int32>(0),
                        userid = await reader.GetFieldValueAsync<Int32>(1),
                        urlcheck = await reader.GetFieldValueAsync<string>(2),
                        createdate = await reader.GetFieldValueAsync<DateTime>(3),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(4),
                        status = await reader.GetFieldValueAsync<int>(5),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
        private async Task<List<CustomerDetail>> ReadCustomerDetial(DbDataReader reader)
        {
            var posts = new List<CustomerDetail>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new CustomerDetail(Db)
                    {
                        id = await reader.GetFieldValueAsync<Int32>(0),
                        email = await reader.GetFieldValueAsync<string>(1),
                        nameuser = await reader.GetFieldValueAsync<string>(3),
                        phone = await reader.GetFieldValueAsync<string>(4),
                        homenumber = await reader.GetFieldValueAsync<string>(5),
                        subdistrict = await reader.GetFieldValueAsync<string>(6),
                        district = await reader.GetFieldValueAsync<string>(7),
                        province = await reader.GetFieldValueAsync<string>(8),
                        postcode = await reader.GetFieldValueAsync<string>(9),
                        createdate = await reader.GetFieldValueAsync<DateTime>(10),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(11),
                        role = await reader.GetFieldValueAsync<int>(12),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}