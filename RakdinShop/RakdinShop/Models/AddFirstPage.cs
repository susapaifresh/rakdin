﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Text;

namespace RakdinShop.Models
{
    public class AddFirstPage
    {
        public int id { get; set; }
        public string aboutUs { get; set; }
        public string imageBanner1 { get; set; }
        public string imageBanner2 { get; set; }
        public string imageBanner3 { get; set; }
        public string imageBanner4 { get; set; }

        [JsonIgnore]
        public AppDb Db { get; set; }

        public AddFirstPage(AppDb db = null)
        {
            Db = db;
        }
       
        public async Task<int> CreateFirstPage()
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                //cmd.CommandText = @"INSERT INTO `firstpage`(`aboutus`, `imagebanner1`, `imagebanner2`, `imagebanner3`, `imagebanner4`)
                //        VALUES (@aboutUs, @imageBanner1, @imageBanner2, @imageBanner3, @imageBanner4)";
                cmd.CommandText = @"INSERT INTO `firstpage`(`aboutus`, `imagebanner1`)
                        VALUES (@aboutUs, @imageBanner1)";
                BindParams(cmd);
                int id = await cmd.ExecuteNonQueryAsync();
                    id = (int)cmd.LastInsertedId;
                try
                {
                    //cmd.CommandText = @"INSERT INTO `firstpage`(`aboutus`, `imagebanner1`, `imagebanner2`, `imagebanner3`, `imagebanner4`)
                    //        VALUES (@aboutUs, @imageBanner1, @imageBanner2, @imageBanner3, @imageBanner4)";
                    cmd.CommandText = @"UPDATE `firstpage` SET `imagebanner2` = @imageBanner2  WHERE `id` ="+ id;
                    BindParams(cmd);
                    int id1 = await cmd.ExecuteNonQueryAsync();

                    try
                    {
                        //cmd.CommandText = @"INSERT INTO `firstpage`(`aboutus`, `imagebanner1`, `imagebanner2`, `imagebanner3`, `imagebanner4`)
                        //        VALUES (@aboutUs, @imageBanner1, @imageBanner2, @imageBanner3, @imageBanner4)";
                        cmd.CommandText = @"UPDATE `firstpage` SET `imagebanner3` = @imageBanner3  WHERE `id` =" + id;
                        BindParams(cmd);
                        int id2 = await cmd.ExecuteNonQueryAsync();

                        try
                        {
                            //cmd.CommandText = @"INSERT INTO `firstpage`(`aboutus`, `imagebanner1`, `imagebanner2`, `imagebanner3`, `imagebanner4`)
                            //        VALUES (@aboutUs, @imageBanner1, @imageBanner2, @imageBanner3, @imageBanner4)";
                            cmd.CommandText = @"UPDATE `firstpage` SET `imagebanner4` = @imageBanner4  WHERE `id` =" + id;
                            BindParams(cmd);
                            int id3 = await cmd.ExecuteNonQueryAsync();
                            return id;
                        }
                        catch
                        {
                            return -1;
                            throw;
                        }
                    }
                    catch
                    {
                        return -2;
                        throw;
                    }
                }
                catch
                {
                    return -3;
                    throw;
                }
            }
            catch
            {
                return -4;
                throw;
            }
            // return id = (int)cmd.LastInsertedId;
        }
       
        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
        }
        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@aboutUs",
                DbType = DbType.String,
                Value = aboutUs,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@imageBanner1",
                DbType = DbType.String,
                Value = imageBanner1,
            });
            cmd.Parameters.Add(new MySqlParameter
            {  

            ParameterName = "@imageBanner2",
                DbType = DbType.String,
                Value = imageBanner2,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@imageBanner3",
                DbType = DbType.String,
                Value = imageBanner3,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@imageBanner4",
                DbType = DbType.String,
                Value = imageBanner4,
            });
           
                   }
    }
}
