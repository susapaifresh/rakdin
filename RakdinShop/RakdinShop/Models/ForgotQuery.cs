﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;
using System.Text.RegularExpressions;

namespace RakdinShop.Models
{
    public class ForgotQuery
    {
        public readonly AppDb Db;
        public ForgotQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<CustomerDetail> FindCustomerByEmail(string email)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM `customer` WHERE `email` = @email";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@email",
                DbType = DbType.String,
                Value = email,
            });
            var result = await ReadCustomerDetial(await cmd.ExecuteReaderAsync());

            if (result.Count >0)
            {
                String timeStamp = GetTimestamp(DateTime.Now);
                String urlencode = Base64Encode(result[0].email+ timeStamp);

                Regex reg = new Regex("[*'\",_&#^@=]");
                var str = reg.Replace(urlencode, string.Empty);
                

                cmd.CommandText = @"INSERT INTO `forgot`(`userid`, `urlcheck`) VALUES (" + result[0].id+ ", '" + str + "')";

                try
                {
                    int id = await cmd.ExecuteNonQueryAsync();
                     id = (int)cmd.LastInsertedId;
                    result[0].phone = str;
                    return result[0];
                }
                catch (Exception e)
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        private async Task<List<CustomerDetail>> ReadCustomerDetial(DbDataReader reader)
        {
            var posts = new List<CustomerDetail>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new CustomerDetail(Db)
                    {
                        id = await reader.GetFieldValueAsync<Int32>(0),
                        email = await reader.GetFieldValueAsync<string>(1),
                        nameuser = await reader.GetFieldValueAsync<string>(3),
                        phone = await reader.GetFieldValueAsync<string>(4),
                        homenumber = await reader.GetFieldValueAsync<string>(5),
                        subdistrict = await reader.GetFieldValueAsync<string>(6),
                        district = await reader.GetFieldValueAsync<string>(7),
                        province = await reader.GetFieldValueAsync<string>(8),
                        postcode = await reader.GetFieldValueAsync<string>(9),
                        createdate = await reader.GetFieldValueAsync<DateTime>(10),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(11),
                        role = await reader.GetFieldValueAsync<int>(12),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}