﻿using System;
using System.Data;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using RakdinShop.Performance;
using System.Text;

namespace RakdinShop.Models
{
    public class Address
    {
       
        public string homenumber { get; set; }
        public string subdistrict { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string postcode { get; set; }

        [JsonIgnore]
        public AppDb Db { get; set; }

        public Address(AppDb db = null)
        {
            Db = db;
        }
     
        

        public async Task<int> UpdateAsync(string id,string homenumber, string subdistrict, string district, string province, string postcode, string addNo)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                if (addNo == "1")
                {
                    cmd.CommandText = @"UPDATE `customer` SET `homenumber` = @homenumber, `subdistrict` = @subdistrict , `district` = @district, `province` = @province , `postcode` = @postcode  WHERE `Id` = @id;";

                }
                if (addNo == "2")
                {
                    cmd.CommandText = @"UPDATE `customer` SET `homenumber2` = @homenumber, `subdistrict2` = @subdistrict , `district2` = @district, `province2` = @province , `postcode2` = @postcode  WHERE `Id` = @id;";
                   
                }
                if (addNo == "3")
                {
                    cmd.CommandText = @"UPDATE `customer` SET `homenumber3` = @homenumber, `subdistrict3` = @subdistrict , `district3` = @district, `province3` = @province , `postcode3` = @postcode  WHERE `Id` = @id;";
                }

                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@homenumber",
                    DbType = DbType.String,
                    Value = homenumber,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@subdistrict",
                    DbType = DbType.String,
                    Value = subdistrict,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@district",
                    DbType = DbType.String,
                    Value = district,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@province",
                    DbType = DbType.String,
                    Value = province,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@postcode",
                    DbType = DbType.String,
                    Value = postcode,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.String,
                    Value = id,
                });
                await cmd.ExecuteNonQueryAsync();
                return 200;
            }
            catch (Exception e)
            {
                return 404;
            }
        }
    }
}
