﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;
using Newtonsoft.Json;

namespace RakdinShop.Models
{
    public class ReceiptQuery
    {

        public readonly AppDb Db;
        public ReceiptQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<int> UpdateAsync(string id,int status)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `order` SET `status` = @status  WHERE `id` = @id";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.String,
                    Value = id,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@status",
                    DbType = DbType.Int32,
                    Value = status,
                });
                await cmd.ExecuteNonQueryAsync();
                return 200;
            }
            catch
            {
                return 404;

            }

        }
        

//        public async Task<List<DataReceiptModel>> FindReceiptById(String id)
//        {
//            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
//            cmd.CommandText = @"SELECT `order`.id ,`order`.`productname` 
//,`order`.`price`,`order`.`quantity`,`customer`.`nameuser`
//, `order`.`status`, `order`.`createdate`,`order`.`userid`
//FROM (`order` INNER JOIN `customer` ON `order`.`userid` = `customer`.id) WHERE `order`.`id` = @id";
//            cmd.Parameters.Add(new MySqlParameter
//            {
//                ParameterName = "@id",
//                DbType = DbType.String,
//                Value = id,
//            });
//            return await ReadOrderDetialAsync(await cmd.ExecuteReaderAsync());
//        }

        public async Task<List<DataReceiptModel>> FindReceipByDate(String startdate, String enddate, String orderID)
        {
               var cmd = Db.Connection.CreateCommand() as MySqlCommand;

            if (orderID != "")
            {
                cmd.CommandText = @"SELECT `orderreceipt`.id ,`orderreceipt`.`orderid` 
,`orderreceipt`.`receiptimage`,`orderreceipt`.`remark`,`orderreceipt`.`datereceipt`
, `orderreceipt`.`status`, `orderreceipt`.`createdate`, `orderreceipt`.`updatedate`,`orderreceipt`.`userid`,`customer`.`nameuser`,`orderreceipt`.`amount`
FROM (`orderreceipt` INNER JOIN `customer` ON `orderreceipt`.`userid` = `customer`.id)
WHERE `orderreceipt`.`createdate` BETWEEN @startdate AND  @enddate and `orderreceipt`.orderid = @orderID";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@startdate",
                    DbType = DbType.String,
                    Value = startdate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@enddate",
                    DbType = DbType.String,
                    Value = enddate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@orderID",
                    DbType = DbType.String,
                    Value = orderID,

                });
            }
            else
            {
                cmd.CommandText = @"SELECT `orderreceipt`.id ,`orderreceipt`.`orderid` 
,`orderreceipt`.`receiptimage`,`orderreceipt`.`remark`,`orderreceipt`.`datereceipt`
, `orderreceipt`.`status`, `orderreceipt`.`createdate`, `orderreceipt`.`updatedate`,`orderreceipt`.`userid`,`customer`.`nameuser`,`orderreceipt`.`amount`
FROM (`orderreceipt` INNER JOIN `customer` ON `orderreceipt`.`userid` = `customer`.id)
WHERE `orderreceipt`.`createdate` BETWEEN @startdate AND  @enddate";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@startdate",
                    DbType = DbType.String,
                    Value = startdate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@enddate",
                    DbType = DbType.String,
                    Value = enddate,

                });
            }
            

            return await ReadReceiptReportAsync(await cmd.ExecuteReaderAsync());
        }
       
        private async Task<List<DataReceiptModel>> ReadReceiptReportAsync(DbDataReader reader)
        {
            var posts = new List<DataReceiptModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new DataReceiptModel(Db)
                    {

                        id = await reader.GetFieldValueAsync<int>(0),
                        orderid = await reader.GetFieldValueAsync<string>(1),
                        receiptimage = await reader.GetFieldValueAsync<string>(2),
                        remark = await reader.GetFieldValueAsync<string>(3),
                        datereceipt = await reader.GetFieldValueAsync<DateTime>(4),
                        status = await reader.GetFieldValueAsync<int>(5),
                        createdate = await reader.GetFieldValueAsync<DateTime>(6),
                        updatedate = await reader.GetFieldValueAsync<DateTime>(7),
                        userid = await reader.GetFieldValueAsync<int>(8),
                        nameuser = await reader.GetFieldValueAsync<string>(9),
                        amount = await reader.GetFieldValueAsync<string>(10),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
        //private async Task<List<DataReportModel>> ReadReceiptDetialAsync(DbDataReader reader)
        //{
        //    var posts = new List<DataReportModel>();
        //    using (reader)
        //    {
        //        while (await reader.ReadAsync())
        //        {
        //            var post = new DataReportModel(Db)
        //            {
        //                id = await reader.GetFieldValueAsync<string>(0),
        //                productname = await reader.GetFieldValueAsync<string>(1),
        //                price = await reader.GetFieldValueAsync<int>(2),
        //                quantity = await reader.GetFieldValueAsync<int>(3),
        //                nameuser = await reader.GetFieldValueAsync<string>(4),
        //                status = await reader.GetFieldValueAsync<int>(5),
        //                createdate = await reader.GetFieldValueAsync<DateTime>(6),
        //                userid = await reader.GetFieldValueAsync<int>(7),
        //            };
        //            posts.Add(post);
        //        }
        //    }
        //    return posts;
        //}
    }


    public class DateQueryReceiptModel
    {
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string orderID { get; set; }
    }
    public class UpdateReceiptModel
    {
        public string id { get; set; }
        public int status { get; set; }

    }
    public class DataReceiptModel
    {
        public int id { get; set; }
        public string orderid { get; set; }
        public string receiptimage { get; set; }
        public string remark { get; set; }
        public DateTime datereceipt { get; set; }
        public int status { get; set; }
        public int userid { get; set; }
        public string nameuser { get; set; }
        public string amount { get; set; }
        public DateTime createdate { get; set; }
        public DateTime updatedate { get; set; }

        [JsonIgnore]
        public AppDb Db { get; set; }

        public DataReceiptModel(AppDb db = null)
        {
            Db = db;
        }
        
    }
}