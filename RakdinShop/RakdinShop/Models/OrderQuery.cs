﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Text;

namespace RakdinShop.Models
{
    public class OrderQuery
    {

        public readonly AppDb Db;
        public OrderQuery(AppDb db)
        {
            Db = db;
        }

        public String getStatusText(int id)
        {
            switch (id)
            {
                case 0:
                    return "ยังไม่ชำระ";

                case 1:
                    return "ยืนยันการชำระเงิน";

                case 2:
                    return "จัดส่งสินค้า";

                case 3:
                    return "ผู้สั่งได้รับสินค้าเรัยบร้อย";
                    
                default:
                    return "ยังไม่ชำระ";
                    
            }
        }
        
        public int sendStatusEmail(String email,String id,int status,String updateby)
        {
            try
            {

                // Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                client.Port = 25;
                client.UseDefaultCredentials = false;
                client.Host = "mail.tsdevelopment.co.th";
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Credentials = new System.Net.NetworkCredential("tsdbackup@tsdevelopment.co.th", "D3v0t3D%$");

                MailMessage mm = new MailMessage("tsdbackup@tsdevelopment.co.th", email, "Rakdin oreder update", "Your order ID:" + id+ " สถานะคำสั่งซื้อ "+ this.getStatusText(status));
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);

                return 200;
            }
            catch (Exception e)
            {
                return 405;
            }
        }
        public async Task<int> UpdateAsync(string id,int status, string email, string updateby)
        {
            try
            {
                var cmd = Db.Connection.CreateCommand() as MySqlCommand;
                cmd.CommandText = @"UPDATE `order` SET `status` = @status , `lastupdate` = @updateby WHERE `id` = @id";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@id",
                    DbType = DbType.String,
                    Value = id,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@status",
                    DbType = DbType.Int32,
                    Value = status,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@updateby",
                    DbType = DbType.String,
                    Value = updateby,
                }); await cmd.ExecuteNonQueryAsync();
                
              try
                {
                    cmd = Db.Connection.CreateCommand() as MySqlCommand;
                    cmd.CommandText = @"INSERT INTO `orderlog`(`orderid`, `status`, `updateby`)
                        VALUES (@orderid, @status, @updateby)";
                    cmd.Parameters.Add(new MySqlParameter
                    {
                        ParameterName = "@orderid",
                        DbType = DbType.String,
                        Value = id,
                    });
                    cmd.Parameters.Add(new MySqlParameter
                    {
                        ParameterName = "@status",
                        DbType = DbType.Int32,
                        Value = status,
                    });
                    cmd.Parameters.Add(new MySqlParameter
                    {
                        ParameterName = "@updateby",
                        DbType = DbType.String,
                        Value = updateby,
                    }); await cmd.ExecuteNonQueryAsync();

                    return this.sendStatusEmail(email, id, status, updateby);
                }
                catch
                {
                    return 404;
                }
            }
            catch
            {
                return 404;

            }

        }

        public async Task<List<OrderActiveListModel>> GetOrderListActive(String id)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT DISTINCT (id) FROM `order` WHERE `status` = 0 AND `userid` = @id";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.String,
                Value = id,
            });
            return await ReadOrderActiveListAsync(await cmd.ExecuteReaderAsync());
        }

        public async Task<List<DataReportModel>> FindOrderById(String id)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT `order`.id ,`order`.`productname` 
,`order`.`price`,`order`.`quantity`,`customer`.`nameuser`
, `order`.`status`, `order`.`createdate`,`order`.`userid`,`order`.`address`,`customer`.`email`
FROM (`order` INNER JOIN `customer` ON `order`.`userid` = `customer`.id) WHERE `order`.`id` = @id";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.String,
                Value = id,
            });
            return await ReadOrderDetialAsync(await cmd.ExecuteReaderAsync());
        }

        public async Task<List<DataReportModel>> FindOrderByDate(String startdate, String enddate, String orderID, String customerID)
        {
         
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            if (customerID != "")
            {
                cmd.CommandText = @"
SELECT `order`.id ,`order`.`productname` 
,`order`.`price`,`order`.`quantity`,`customer`.`nameuser`
, `order`.`status`, `order`.`createdate`
FROM (`order` INNER JOIN `customer` ON `order`.`userid` = `customer`.id)
WHERE `order`.`createdate` BETWEEN @startdate AND  @enddate and `order`.userid = @customerID";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@startdate",
                    DbType = DbType.String,
                    Value = startdate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@enddate",
                    DbType = DbType.String,
                    Value = enddate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@orderID",
                    DbType = DbType.String,
                    Value = orderID,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@customerID",
                    DbType = DbType.String,
                    Value = customerID,

                });
            }
           else if (orderID != "")
            {
                cmd.CommandText = @"
SELECT `order`.id ,`order`.`productname` 
,`order`.`price`,`order`.`quantity`,`customer`.`nameuser`
, `order`.`status`, `order`.`createdate`
FROM (`order` INNER JOIN `customer` ON `order`.`userid` = `customer`.id)
WHERE `order`.`createdate` BETWEEN @startdate AND  @enddate and `order`.id = @orderID";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@startdate",
                    DbType = DbType.String,
                    Value = startdate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@enddate",
                    DbType = DbType.String,
                    Value = enddate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@orderID",
                    DbType = DbType.String,
                    Value = orderID,

                });
            }
            else
            {
                cmd.CommandText = @"
SELECT `order`.id ,`order`.`productname` 
,`order`.`price`,`order`.`quantity`,`customer`.`nameuser`
, `order`.`status`, `order`.`createdate`
FROM (`order` INNER JOIN `customer` ON `order`.`userid` = `customer`.id)
WHERE `order`.`createdate` BETWEEN @startdate AND  @enddate";
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@startdate",
                    DbType = DbType.String,
                    Value = startdate,

                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@enddate",
                    DbType = DbType.String,
                    Value = enddate,

                });
            }
           

            return await ReadOrderReportAsync(await cmd.ExecuteReaderAsync());
        }
       
        private async Task<List<DataReportModel>> ReadOrderReportAsync(DbDataReader reader)
        {
            var posts = new List<DataReportModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new DataReportModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<string>(0),
                        productname = await reader.GetFieldValueAsync<string>(1),
                        price = await reader.GetFieldValueAsync<float>(2),
                        quantity = await reader.GetFieldValueAsync<int>(3),
                        nameuser = await reader.GetFieldValueAsync<string>(4),
                        status = await reader.GetFieldValueAsync<int>(5),
                        createdate = await reader.GetFieldValueAsync<DateTime>(6),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
        private async Task<List<DataReportModel>> ReadOrderDetialAsync(DbDataReader reader)
        {
            var posts = new List<DataReportModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new DataReportModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<string>(0),
                        productname = await reader.GetFieldValueAsync<string>(1),
                        price = await reader.GetFieldValueAsync<float>(2),
                        quantity = await reader.GetFieldValueAsync<int>(3),
                        nameuser = await reader.GetFieldValueAsync<string>(4),
                        status = await reader.GetFieldValueAsync<int>(5),
                        createdate = await reader.GetFieldValueAsync<DateTime>(6),
                        userid = await reader.GetFieldValueAsync<int>(7),
                        address = await reader.GetFieldValueAsync<string>(8),
                        email = await reader.GetFieldValueAsync<string>(9),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
        private async Task<List<OrderActiveListModel>> ReadOrderActiveListAsync(DbDataReader reader)
        {
            var posts = new List<OrderActiveListModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new OrderActiveListModel(Db)
                    {
                        id = await reader.GetFieldValueAsync<string>(0),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }


    public class DateQueryReportModel
    {
        public string startdate { get; set; }
        public string enddate { get; set; }
        public string orderID
        {
            get; set;
        }
        public string customerID
        {
            get; set;
        }
    }
    public class UpdateReportModel
    {
        public string id { get; set; }
        public int status { get; set; }
        public string email { get; set; }
        public string updateby { get; set; }

    }
    public class OrderActiveListModel
    {
        public string id { get; set; }
        [JsonIgnore]
        public AppDb Db { get; set; }

        public OrderActiveListModel(AppDb db = null)
        {
            Db = db;
        }
    }
    public class DataReportModel
    {
        public string id { get; set; }
        public string productname { get; set; }
        public float price { get; set; }
        public float quantity { get; set; }
        public string nameuser { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public int status { get; set; }
        public int userid { get; set; }
        public DateTime createdate { get; set; }

        [JsonIgnore]
        public AppDb Db { get; set; }

        public DataReportModel(AppDb db = null)
        {
            Db = db;
        }
        
    }
}