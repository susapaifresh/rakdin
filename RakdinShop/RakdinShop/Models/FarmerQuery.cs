﻿using RakdinShop.Performance;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using System.Data.Common;
using MySql.Data.MySqlClient;
using System;

namespace RakdinShop.Models
{
    public class FarmerQuery
    {

        public readonly AppDb Db;
        public FarmerQuery(AppDb db)
        {
            Db = db;
        }

        public async Task<Farmer> FindFarmerByIDAsync(int id)
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"SELECT * FROM `farmer` WHERE `id` = @id";
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                DbType = DbType.Int32,
                Value = id,
            });
            var result = await ReadFarmerDetialAsync(await cmd.ExecuteReaderAsync());
            return result.Count > 0 ? result[0] : null;
        }


        public async Task<List<Farmer>> GetAllFarmerAsync()
        {
            var cmd = Db.Connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `farmer` ORDER BY `id`;";
            return await ReadFarmerDetialAsync(await cmd.ExecuteReaderAsync());
        }

        //public async Task DeleteAllAsync()
        //{
        //    var txn = await Db.Connection.BeginTransactionAsync();
        //    try
        //    {
        //        var cmd = Db.Connection.CreateCommand();
        //        cmd.CommandText = @"DELETE FROM `BlogPost`";
        //        await cmd.ExecuteNonQueryAsync();
        //        await txn.CommitAsync();
        //    }
        //    catch
        //    {
        //        await txn.RollbackAsync();
        //        throw;
        //    }
        //}

        private async Task<List<Farmer>> ReadFarmerDetialAsync(DbDataReader reader)
        {
            var posts = new List<Farmer>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new Farmer(Db)
                    {
                        id = await reader.GetFieldValueAsync<int>(0),
                        name = await reader.GetFieldValueAsync<string>(1),
                        sector = await reader.GetFieldValueAsync<string>(2),
                        detail = await reader.GetFieldValueAsync<string>(3),
                        imageprofile = await reader.GetFieldValueAsync<string>(6),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}